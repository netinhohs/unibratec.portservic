<?
require_once("DB.php");
class EstadosModel {
	
	public function __construct() {

    }
	
	public function listEstadosAll(){
		$data = array();
		$sql = "SELECT * FROM estado";
		$consulta = DB::prepare($sql);
		$consulta->execute();
		while ($obj = $consulta->fetch(PDO::FETCH_ASSOC)){
			$obj["servicos"] = $this->listCountServicos($obj['uf']);
			array_push($data,$obj);
		}
		return $data;
	}
	
	public function listCountServicos($uf){
		$sql = "SELECT count(*) as qtd FROM servico where uf = :uf and status = 'A'";
		//return "SELECT count(*) as qtd FROM servico where uf = $uf";
		$consulta = DB::prepare($sql);
		$consulta->bindParam(":uf",$uf);
		$consulta->execute();
		$qtd = $consulta->fetch(PDO::FETCH_ASSOC);
		return $qtd["qtd"];
	}
	
} 