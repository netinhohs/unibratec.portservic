<?
require_once("DB.php");
class ServicoImagenModel {
	
	public function __construct() {

    }

	public function uploadServico(){
		$data = array();
		try{
			$mUploadDir = "../web/uploadFile/";
			$data['debug'] = is_dir($mUploadDir);
			$data['mydir'] = $_SERVER['PHP_SELF'];
			foreach($_FILES as $mFile) {
				if (!empty($mFile["size"])) {
					// Convert bytes to megabytes
					$fileSize = ($mFile["size"] / 1024) / 1024;
					
					if ($fileSize > 2) {
						$data["success"] = false;
						$data["error"] = "Arquivo maior do que 2 MB";
						return $data;
						//throw new Exception("Arquivo maior do que 2 MB");
					}
				} else {
					$data["success"] = false;
					$data["error"] = "Arquivo maior do que 2 MB ou corrompido";
					return $data;
					//throw new Exception("Arquivo maior do que 2 MB ou corrompido");
				}
				$extencions = array("jpg","gif","png");
				$type = explode(".",$mFile["name"]);
				//$name = $type[0]."_".$request['id'].".".$type[1];
				if(move_uploaded_file($mFile["tmp_name"], $mUploadDir.$mFile["name"])) {
					//$sql = "update usuario set foto = :foto where id = :id;";
					//$insert = DB::prepare($sql);
					//$insert->bindParam(":foto",$name);
					//$insert->bindParam(":id",$request['id']);
					//$insert->execute();
					$data["success"] = true;
					$data["ServicoImagem"] = $_FILES;
				}else{
					$data["success"] = false;
					$data["error"] = "diretorio não existe";
				}
			}

		}catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;		
	}
	
	public function listServico_imagens(){
		$sql = "select * from servico_imagem";
		$consulta = DB::prepare($sql);
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function listServico_imagensById($request){
		$sql = "select * from servico_imagem where id = :id";
		$consulta = DB::prepare($sql);
		$consulta->bindParam(":id",$request['id']);
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function insertServico_imagens($request){
		$data = array();
		try{
			$sql = "insert into servico_imagem (imagem,status,servico_id) 
			values (:imagem,:status,:servico_id);";
			$insert = DB::prepare($sql);
			$insert->bindParam(":imagem",$request['imagem']);
			$insert->bindParam(":status",$request['status']);
			$insert->bindParam(":servico_id",$request['servico_id']);
			$insert->execute();
			$data["success"] = true;
		}catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;
	}
	
	
	public function updateServico_imagens($request){
		$data = array();
		try{
			$sql = "update servico_imagem set imagem = :imagem,status = :status ,servico_id = :servico_id  where id = :id;";
			$insert = DB::prepare($sql);
			$insert->bindParam(":imagem",$request['imagem']);
			$insert->bindParam(":status",$request['status']);
			$insert->bindParam(":servico_id",$request['servico_id']);
			$insert->bindParam(":id",$request['id']);
			$insert->execute();
			$data["success"] = true;
		}catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;		
	}
	
	public function delteServico_imagensById($request){
		$data = array();
		try{
			$sql = "delete from servico_imagem where id = :id";
			$consulta = DB::prepare($sql);
			$consulta->bindParam(":id",$request['id']);
			$consulta->execute();
			$data["success"] = true;
		}catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;
	}	
	
}