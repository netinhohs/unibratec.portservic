<?
require_once("DB.php");
class UsuarioModel{

    public function __construct() {
    }
	
	public function listUsuarios(){
		$sql = "select * from usuario";
		$consulta = DB::prepare($sql);
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function inserirFotoPerfil($request){
		$data = array();
		try{
			$mUploadDir = "../web/uploadFile/";
			$data['debug'] = is_dir($mUploadDir);
			$data['mydir'] = $_SERVER['PHP_SELF'];
			foreach($_FILES as $mFile) {
				if (!empty($mFile["size"])) {
					// Convert bytes to megabytes
					$fileSize = ($mFile["size"] / 1024) / 1024;
					
					if ($fileSize > 2) {
						$data["success"] = false;
						$data["error"] = "Arquivo maior do que 2 MB";
						return $data;
						//throw new Exception("Arquivo maior do que 2 MB");
					}
				} else {
					$data["success"] = false;
					$data["error"] = "Arquivo maior do que 2 MB ou corrompido";
					return $data;
					//throw new Exception("Arquivo maior do que 2 MB ou corrompido");
				}
				$extencions = array("jpg","gif","png");
				$type = explode(".",$mFile["name"]);
				$name = $type[0]."_".$request['id'].".".$type[1];
				if(move_uploaded_file($mFile["tmp_name"], $mUploadDir.$name)) {
					$sql = "update usuario set foto = :foto where id = :id;";
					$insert = DB::prepare($sql);
					$insert->bindParam(":foto",$name);
					$insert->bindParam(":id",$request['id']);
					$insert->execute();
					$data["success"] = true;
					$data["User"] = $request;
				}else{
					$data["success"] = false;
					$data["error"] = "diretorio n�o existe";
				}
			}

		}catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;		
	}
	
	public function logar($request){
		$data = array();
		try{
			$sql = "SELECT * FROM usuario WHERE email = :email AND senha = :senha ";
			$consulta = DB::prepare($sql);
			$consulta->bindParam(":email",$request['email']);
			$consulta->bindParam(":senha",$request['senha']);
			$consulta->execute();
			//$data["success"] = true;
			$data["User"] = $consulta->fetchAll(PDO::FETCH_ASSOC);
			$data["Adress"] = $this->enderecoById($data["User"][0]);
			//var_dump($consulta);
			if(count($data["User"]) > 0){
				$data["success"] = true;
			}else{
				$data["success"] = false;
				$data["teste"] = "select * from usuario where email = ".$request['email']." and senha = ".$request['senha'];
			}
		}catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;
	}

	public function logarAdmin($request){
		$data = array();
		try{
			$sql = "SELECT * FROM usuario WHERE email = :email AND senha = :senha AND id_perfil = 2 AND status = 'A' ";
			$consulta = DB::prepare($sql);
			$consulta->bindParam(":email",$request['email']);
			$consulta->bindParam(":senha",$request['senha']);
			$consulta->execute();
			//$data["success"] = true;
			$data["dados"] = $consulta->fetch(PDO::FETCH_ASSOC);
			if($data["dados"] == false){
				$data = array();
			}
		}catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;
	}

	public function listUsuariosById($request){
		$sql = "select * from usuario where id = :id";
		$consulta = DB::prepare($sql);
		$consulta->bindParam(":id",$request['id']);
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);
	}


	public function listServicosUsuario(){
		$sql = "select * from usuario u , servico s where s.usuario_id = u.id";
		$consulta = DB::prepare($sql);
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);
	}

	public function listServicosUsuarioById($request){
		$sql = "select * from usuario u, servico s where s.usuario_id = u.id and u.id = :id";
		$consulta = DB::prepare($sql);
		$consulta->bindParam(":id",$request['id']);
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function insertUsuario($request){
		$data = array();
		try{
			$sql = "insert into usuario (email,senha,nome,
			cpf_cnpj,tipo,status,nick) 
			values (:email,:senha,:nome,:cpf_cnpj,:tipo,:status,:nick);";
			$insert = DB::prepare($sql);
			$insert->bindParam(":email",$request['email']);
			$insert->bindParam(":senha",$request['senha']);
			$insert->bindParam(":nome",$request['nome']);
			$insert->bindParam(":cpf_cnpj",$request['cpf_cnpj']);
			$insert->bindParam(":tipo",$request['tipo']);
			$insert->bindParam(":status",$request['status']);
			$insert->bindParam(":nick",$request['nick']);
			$insert->execute();
			$request["id"] = DB::lastInsertId();
			$data["success"] = true;
			$data["Adress"] = $this->enderecoById($request);
			$data["User"] = $request;
		}catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;
		//return $request;
	}
	
	public function enderecoById($request){
		$sql = "select * from endereco where usuario_id = :id";
		$consulta = DB::prepare($sql);
		$consulta->bindParam(":id",$request['id']);
		$consulta->execute();
		$return = $consulta->fetchAll(PDO::FETCH_ASSOC);
		if(count($return) > 0){
			return $return;
		}else{
			return array("id"=>"","endereco"=>"","numero"=>"","bairro"=>"","cep"=>"","cidade"=>"","uf"=>"","usuario_id"=>"");
		}
	}	
	
	public function updateUsuario($request){
		$data = array();
		try{
			$sql = "update usuario set email = :email,senha = :senha ,nome = :nome,
			cpf_cnpj = :cpf_cnpj,tipo = :tipo, status = :status, nick = :nick, 
			sobre = :sobre, telefone = :telefone, celular = :celular, foto = :foto, 
			sexo = :sexo where id = :id;";
			$insert = DB::prepare($sql);
			$insert->bindParam(":email",$request['email']);
			$insert->bindParam(":senha",$request['senha']);
			$insert->bindParam(":sobre",$request['sobre']);
			$insert->bindParam(":nome",$request['nome']);
			$insert->bindParam(":cpf_cnpj",$request['cpf_cnpj']);
			$insert->bindParam(":tipo",$request['tipo']);
			$insert->bindParam(":status",$request['status']);
			$insert->bindParam(":nick",$request['nick']);
			$insert->bindParam(":telefone",$request['telefone']);
			$insert->bindParam(":celular",$request['celular']);
			$insert->bindParam(":foto",$request['foto']);
			$insert->bindParam(":sexo",$request['sexo']);
			$insert->bindParam(":id",$request['id']);
			$insert->execute();
			$data["success"] = true;
			$data["usuario"] = $request;
		}catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;		
	}
	
	public function delteUsuariosById($request){
		$data = array();
		try{
			$sql = "delete from usuario where id = :id";
			$consulta = DB::prepare($sql);
			$consulta->bindParam(":id",$request['id']);
			$consulta->execute();
			$data["success"] = true;
		}catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;
	}	

public function trocarSenha($request){
		$data = array();
		try{
			$sql = "UPDATE usuario SET senha = :novaSenha where id = :id";
			$consulta = DB::prepare($sql);
			$consulta->bindParam(":id",$request['id']);
			$consulta->bindParam(":novaSenha",$request['novaSenha']);
			$consulta->execute();
			$data["success"] = true;
		}catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}
		return $data;
	} 

	public function esqueciSenha($request){
		
		$sql = "select * from usuario where email = :email";
		$consulta = DB::prepare($sql);
		$consulta->bindParam(":email",$request['email']);
		$consulta->execute();
		
		return $consulta->fetchAll(PDO::FETCH_ASSOC);
	}	
}