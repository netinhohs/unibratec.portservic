<nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="index.php" class="navbar-brand ">
                <span class="logo"><strong>Port</strong>
                    <span class="handwriting" style="margin-right: 100px;">Servic</span>
                    <br />
                    <small >O melhor jeito de encontrar profissionais de qualidade</small></span>
                </a>
            </div>

            <div class="collapse navbar-collapse">

                <ul class="nav navbar-nav navbar-right visible-xs">

                    <li class="active"><a href="anuncios.php">Serviços</a></li>
                    <?
                    session_start();

                    if(!isset($_SESSION['usuario'])){ ?>
                    <li><a href="register.php">Cadastre-se</a></li>
                    <? } ?>

                    <? if(!isset($_SESSION['usuario'])){ ?>
                    <li><a href="login.php">Entrar</a></li>
                    <? } ?>

                    <? if(isset($_SESSION['usuario'])){ ?>                    
                    <li><a href="dashboard.php">Minha conta</a></li>
                    <? } ?>

                    <? if(isset($_SESSION['usuario'])){ ?>                    
                    <li><a href="sair.php">Sair</a></li>
                    <? } ?>

                    <li><a href="novo-anuncio.php">Inserir Anúncio</a></li>


                </ul> 
                <div class="nav navbar-nav navbar-right hidden-xs">
                    <div class="row">
                        <div class="pull-right">
                            <a href="anuncios.php">Serviços</a> |
                            <?
                            session_start();

                            if(!isset($_SESSION['usuario'])){ ?>
                            <a href="register.php">Cadastre-se</a> |
                            <? } ?>

                            <? if(!isset($_SESSION['usuario'])){ ?>
                            <a data-toggle="modal" data-target="#modalLogin"  href="#" id="modalLoginMenu">Entrar</a> |
                            <? } ?>

                            <? if(isset($_SESSION['usuario'])){ ?>
                            <a href="dashboard.php">Minha conta</a> |
                            <? } ?>

                            <? if(isset($_SESSION['usuario'])){ ?>
                            <a href="sair.php">Sair</a> |
                            <? } ?>

                            <a href="novo-anuncio.php" class="btn btn-warning post-ad-btn">Inserir Anúncio</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>


    <? if(!isset($_SESSION['usuario'])){ ?>
    <!-- Modal do login : Inicio -->
    <!-- Modal do login : Inicio -->
    <div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="modalLogin" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="alert alert-danger" id="danger_login" style="display:none;">
                    <strong>Alerta!</strong>
                    <span id="span_alerta_login"></span>
                </div>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">
                        Faça login em sua conta</h4>
                    </div>
                    <div class="modal-body">
                        <p>Se você tem uma conta conosco , por favor, informe seus dados abaixo .</p>
                        <form id="form_login_usuario" accept-charset="UTF-8" class="form ajax" data-replace=".error-message p">
                            <div class="form-group">
                                <input  id="login_senha" placeholder="Seu nome de usuário / e-mail" class="form-control" name="email" type="text">
                            </div>
                            <div class="form-group">
                                <input id="login_email" placeholder="Sua senha" class="form-control" name="senha" type="password" value="">
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-6">
                                    <input id="login_usuario" type="button" class="btn btn-primary pull-right" value="Entrar" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <a data-toggle="modal" href="#modalForgot">Esqueceu sua senha?</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer" style="text-align: center">
                        <div class="error-message"><p style="color: #000; font-weight: normal;">Não tem uma conta? <a class="link-info" href="register.php">Registrar agora</a></p></div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <script src="js/usuario.js"></script>
        <!-- Modal do login : Fim -->
        <? } ?>
        <!-- Modal Esqueceu Senha : inicio-->
        <div class="modal fade" id="modalForgot" tabindex="-1" role="dialog" aria-labelledby="modalForgot" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <div class="alert alert-danger" id="danger_esquecisenha" style="display:none;">
                            <strong>Alerta!</strong>
                            <span id="span_esquecisenha"></span>
                        </div>
                        <div class="alert alert-info" id="esqueci_ok" style="display:none;">
                            <p>Foi enviado um email para sua caixa de entrada.</p>
                        </div>

                        <h4 class="modal-title">Esqueceu sua senha?</h4>
                    </div>
                    <div class="modal-body">
                        <p>Digite seu e-mail para continuar</p>
                        <form role="form" id="formEsqueciSenha">
                            <div class="form-group">
                                <input name="email" id="emailUser" type="email" class="form-control" placeholder="Informe seu email">
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                </div><div class="col-md-6">
                                <input id="EsqueciSenha" type="button" class="btn btn-primary pull-right" value="Continuar" />
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- Modal Esqueceu Senha : Fim-->
    <script>
    $( document ).ready(function() {
        $("#EsqueciSenha").click(function(){
            var formUrlEsenha = $('#formEsqueciSenha').serialize();
            var objUsuario;
            var emailUser  = $('#emailUser').val();
            if ( emailUser != "") {
                $("#danger_esquecisenha").hide();
                $.ajax({
                    url:"http://portservise.esy.es/portservise_webservice/UsuarioModel/esqueciSenha",
                    data:formUrlEsenha,
                    type:"POST",
                    dataType:"JSON",
                    success:function(data){                                      

                        try {
                            $("#danger_esquecisenha").hide();
                            $("#esqueci_ok").show();

                            $.ajax({
                                url:"util/emailEsquciSenha.php",
                                data:{  email  :data[0].email
                                 ,idUser :data[0].id},
                                 type:"POST",
                                 success:function(data){
                                    console.log('ok')
                                },
                                error:function(data){
                                    console.log('erro volta');
                                }
                            });

                        } catch(e) {
                                        // statements
                                        $("#danger_esquecisenha").show();
                                        $("#esqueci_ok").hide();
                                        $("#span_esquecisenha").html("Esse email não existe.");
                                    }

                                },
                                error:function(data){
                                    console.log('e');
                                }           
                            });
}else{
    $("#esqueci_ok").hide();
    $("#danger_esquecisenha").show();
    $("#span_esquecisenha").html("Informe seu email.");
};
});
});
</script>