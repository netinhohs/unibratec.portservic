<?php 
    include 'verificar.php';    
    include 'head.php'; ?>
<body>

    <?php include 'nav_top.php';    ?>
    <hr class="topbar"/>
    <div class="container">
        <div class="row">
            <?php include "menu-dashboard.php" ?>
            <div class="col-sm-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Meus Anúncios</div>
                    <div class="panel-body">
                        <form class="form-vertical">
                            <fieldset>
                                <div class="row">
                                    <div class="col-sm-12" >
                                        
                                        <table class="table edit-listings">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th></th>
                                                    <th>&nbsp;Postado</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbody_MyListAll">
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <br />
    </div>
</div>
<script src="js/servico.js"></script>
<script>
servico.myListAll(<?= $_SESSION['usuario']['id'];?>);
</script>
<?php include 'footer.php'; ?>
</body>
<!-- Mirrored from templates.expresspixel.com/bootlistings/account_ads.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Feb 2016 12:17:51 GMT -->
</html>