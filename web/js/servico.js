var servico = {
	construct:$(function(){
		servico.AjaxLastIdList();
		servico.cadastrarServico();
		servico.buscarCepCorreios();
		servico.initList();
		//servico.myListAll();
		servico.detalhe();
		servico.more();
		servico.filtro();
	}),
	cadastrarServico:function(){
		$(".cadastrar_serico").click(function(){ 
			if($("#categoria_id").val() != ""){
				$("#categoria_id").css({"border-color":"#f2f2f2"});
				if($("#aceita_termos").prop( "checked" )){
					$("#aceita_termos").css({"border-color":"#f2f2f2"});
					if($("#cep").val() != ""){
						$("#cep").css({"border-color":"#f2f2f2"});
						if($("#titulo").val() != ""){
							$("#titulo").css({"border-color":"#f2f2f2"});
							if($("#valor").val() != ""){
								$("#valor").css({"border-color":"#f2f2f2"});
								var url = $("#form_servico").serialize();
								$.ajax({
									url:"http://portservise.esy.es/portservise_webservice/ServicoModel/insertServicos",
									data:url,
									type:"POST",
									dataType:"JSON",
									success:function(data){
										if(data.success){
											for(var i = 0; i < document.querySelectorAll(".fotos").length; i++){
												var array_nome_imagem = document.querySelector("#fotos")[i].value.split(".");
												array_nome_imagem[0] = array_nome_imagem[0]+"_"+$("#usuario_id").val();
												array_nome_imagem = array_nome_imagem.join(".");
												$.ajax({
													url:"http://portservise.esy.es/portservise_webservice/ServicoImagenModel/insertServico_imagens",
													data:{imagem:array_nome_imagem,status:"",servico_id:data.id_servico},
													type:"POST"
												});
											}
											window.location = "meus_anuncios.php";
										}
									},
									error:function(data){
										$("#aviso_servico").css({"display":"block"});
										$("#aviso_span_servico").html("");
									}
								});
							}else{
								$("#valor").css({"border-color":"red"});
								$("#aviso_servico").css({"display":"block"});
								$("#aviso_span_servico").html("É necessario informar o valor");								
							}	
						}else{
							$("#titulo").css({"border-color":"red"});
							$("#aviso_servico").css({"display":"block"});
							$("#aviso_span_servico").html("É necessario informar o titulo");							
						}
					}else{
						//cep
						$("#cep").css({"border-color":"red"});
						$("#aviso_servico").css({"display":"block"});
						$("#aviso_span_servico").html("É necessario inforar o cep");
					}
				}else{
					//termos
					$("#aceita_termos").css({"background-color":"red"});
					$("#aviso_servico").css({"display":"block"});
					$("#aviso_span_servico").html("É necessario concordar com os termos");					
				}	
			}else{
				$("#categoria_id").css({"border-color":"red"});
				$("#aviso_servico").css({"display":"block"});
				$("#aviso_span_servico").html("É necessario inforar a categoria");				
				//categoria
			}
		});
	},
	buscarCepCorreios:function(){
		$("#cep").blur(function(){
			if($(this).val() != ""){
				$.ajax({
					url:"http://clareslab.com.br/ws/cep/json/"+$(this).val()+"/",
					type:"POST",
					dataType:"JSON",
					success:function(data){
						$("#cidade").val(data.cidade);
						$("#uf").val(data.uf);
						$("#endereco").val(data.endereco);
						$("#bairro").val(data.bairro);
						console.log(data);
					},
					error:function(data){

					}
				});	
			}
		});	
	},
	listAll:function(id_servico){
		var url = "id="+id_servico+"&"+$("#filtro_anuncio").serialize();
		$("#tbody_listAll").html("<center><img src='/web/uploadFile/input-spinner.gif' /></center>");
		$.ajax({
			url:"http://portservise.esy.es/portservise_webservice/ServicoModel/listServicosImagem",
			type:"POST",
			dataType:"JSON",
			data:url,
			success:function(data){
				//console.log(data);
				var tr = "";
				for(var i = 0; i < data.length; i++){
					var date = data[i].data_cadastro.split(" ");
					date = date[0].split("-").reverse().join("/");
					var imagem = "";
					try{
						imagem = "/web/uploadFile/"+data[i].fotos[0].imagem;
					}catch(e){
						imagem = "css/images/180x100.png";
					}
					var numero = "";
					try{
						numero = data[i].descricao;
						//console.log(numero.length);
						numero = (numero.length);
					}catch(e){
						
					}
					tr += '<div class="row  listing-row"><div class="col-sm-2"><a style="cursor:pointer;" class="thumbnail " ><img attr_id='+data[i].id_servico+' class="detalhes" alt="176 * 120" src="'+imagem+'"></a>';
					tr += '</div><div class="col-sm-10"><h3><a class="detalhes" attr_id='+data[i].id_servico+' style="cursor:pointer;">'+data[i].titulo+' - <strong>R$'+data[i].valor+'</strong></a></h3>';
					tr += '<p class="muted">Localizado em <strong>'+data[i].endereco+', '+data[i].cidade+' - cep:'+data[i].cep+'</strong></p><p class="muted">Postado em: '+date+' na Categoria <a style="cursor:pointer;" class="underline">'+data[i].categoria_descricao+'</a></p>';
					if(numero > 100){
						tr += '<p>'+(data[i].descricao.substring(0,(numero - 50)).replace(" ","\n")+"...")+'</p>';
					}else{
						tr += '<p>'+(data[i].descricao)+'</p>';
					}
					tr += '<p><span class="classified_links pull-right"><a class="link-info underline" style="cursor:pointer;">Compartilhe</a>&nbsp;<a class="link-info underline" style="cursor:pointer;">Adicione aos favoritos</a>';
					tr += '&nbsp;<a class="link-info underline" attr_id='+data[i].id_servico+' style="cursor:pointer;">Detalhes</a>&nbsp;&nbsp;<a class="link-info underline" style="cursor:pointer;">Contato</a></span></p></div></div>';
					tr += '<div id='+data[i].id_servico+' style="display:none;">'+JSON.stringify(data[i])+'</div>';
					/*tr += '<tr>';
					tr += '<td><img src="'+(data[i].imagem == null ? "css/images/180x100.png" : data[i].imagem)+'" style="width:75px" /></td>';
					tr += '<td class="main-td">';
					tr += '<a href="account_ad_create.html">'+data[i].imagem+' 000000000000000000000000</a>';
					tr += '<a href="#" class="no-views">(11 visualizações)</a><br />';
					tr += '<a class="edit-ad" href="account_ad_create.html">Editar anúncio</a>'
					tr += '<a class="extend-ad" href="#" data-toggle="tooltip" title="Contrate um plano para destacar esse anúncio">Topo </a>';
					tr += '<a class="remove-ad" href="#">Remover anúncio</a>';
					tr += '<p >'+data[i].descricao+' NUll</p>';
					tr += '</td>';
					tr += '<td><small>'+date+'</small>';
					tr += '<br><br>';
					tr += '<b>Publicado</b>';
					tr += '</td>';
					tr += '</tr>';*/
					$('#id_servico').val(data[i].id_servico);
					if(i == 9){
						$("#desce").css({"display":"block"}).attr("more",(data[i].id_servico - 1));
					}
				}
				$("#tbody_listAll").html(tr);
				/*$('#pagination').append('<li><a style="cursor:pointer;">Prev</a></li>');
				var quantidade = data.length;
				var indice = 1;
				var key = 0;
				var objJson;
				while(quantidade >= 1){
					quantidade = quantidade - 10;
					objJson = data;
					objJson = objJson.splice(key,10);
					id = objJson[objJson.length - 1];
					$('#pagination').append('<li '+(indice == 1 ? 'class="active"' : '')+' ><a attr_id="'+id.id_servico+'" style="cursor:pointer;" >'+indice+'</a></li>');
					indice++;
					if(indice == 10){
						break;
					}
				}
				$('#pagination').append('<li><a style="cursor:pointer;">Next</a></li>');*/
				servico.detalhe();
				servico.pesqisar();
			},
			error:function(data){
        
			}
		});
		servico.detalhe();
	},
	myListAll:function(id_usuario){
		$("#tbody_MyListAll").html("<center><img src='/web/uploadFile/input-spinner.gif' /></center>");
		$.ajax({
			url:"http://portservise.esy.es/portservise_webservice/ServicoModel/listServicosByUsuarioId2",
			type:"POST",
			data:{id:id_usuario},
			dataType:"JSON",
			success:function(data){
				//console.log(data);
				var tr = "";
				for(var i = 0; i < data.length; i++){
					var date = data[i].data_cadastro.split(" ");
					date = date[0].split("-").reverse().join("/");
					var imagem = "";
					try{
						imagem = "/web/uploadFile/"+data[i].fotos[0].imagem
					}catch(e){
						imagem = "css/images/180x100.png"
					}

					var situacao;
					switch (data[i].situacao) {
						case 'P':
							situacao = '<span style="color:#F0AD4E">Pendente a aprovação</span>';
							break;
						case 'X':
							situacao = '<span style="color:#D9534F">Excluído</span>';
							break;
						case 'A':
							situacao = '<span style="color:#5CB85C">Publicado</span>';
							break;
						default:
							situacao = '<span style="color:#5BC0DE">Aguarde ...</span>';
							break;
					}

					tr += '<div id='+data[i].id_servico+' style="display:none;">'+JSON.stringify(data[i])+'</div>';
					tr += '<tr>';
					tr += '<td><img attr_id='+data[i].id_servico+' class="detalhes" alt="176 * 120" src="'+imagem+'" style="width:75px" ></td>';
					tr += '<td class="main-td">';
					tr += '<a attr_id='+data[i].id_servico+' class="detalhes" style="cursor:pointer;">'+data[i].titulo+' - <strong>R$'+data[i].valor+'</strong></a>';
					tr += '<a href="#" class="no-views">('+data[i].visualizacao+' visualizações)</a><br />';
					tr += '<span style="color: #999999;">Localizado em <strong>'+data[i].endereco+', '+data[i].cidade+' - cep:'+data[i].cep+'</strong><br>Categoria <a style="cursor:pointer;" class="underline">'+data[i].categoria_descricao+'</a> <br></span>';
					tr += '<a class="edit-ad" href="account_ad_create.html">Editar anúncio</a>'
					tr += '<a class="extend-ad" href="#" data-toggle="tooltip" title="Contrate um plano para destacar esse anúncio">Plano </a>';
					tr += '<a class="remove-ad" href="#">Remover anúncio</a>';
					tr += '<p >'+data[i].descricao+'</p>';
					tr += '</td>';
					tr += '</td>';
					tr += '<td><small>'+date+'</small>';
					tr += '<br><br>';
					tr += '<b>'+situacao+'</b>';
					tr += '</td>';
					tr += '</tr>';
				}
				$("#tbody_MyListAll").html(tr);
				servico.detalhe();
				servico.pesqisar();
			},
			error:function(data){
        
			}
		});
	},
	detalhe:function(){
		$(".detalhes").click(function(){
			if($("#id_user").val() != ""){
				$.ajax({
					url:"http://portservise.esy.es/portservise_webservice/AcessoModel/cadastrarAcessoUsuarioHoje",
					type:"POST",
					dataType:"JSON",
					data:{
						id_usuario:$("#id_user").val(),
						acao:1,
						conteudo:$(this).attr("attr_id")
					}
				});					
			}else{
				$.ajax({
					url:"http://portservise.esy.es/portservise_webservice/AcessoModel/cadastrarAcessoHoje",
					type:"POST",
					dataType:"JSON",
					data:{
						acao:1,
						conteudo:$(this).attr("attr_id")
					}
				});
			}
			var idJosn = $(this).attr("attr_id");
			var content = $('#'+idJosn).html();
			var json = JSON.parse(content);
			$("body").append("<form action='details.php' method='POST' id='submeter'><input type='text' name='json' id='json' value='"+content+"' /></form>");
			$("#submeter").submit();
			//window.location = "teste.php";
		});
	},
	pesqisar:function(){
		$(".search-btn").click(function(){
			if($("#id_user").val() != ""){
				$.ajax({
					url:"http://portservise.esy.es/portservise_webservice/AcessoModel/cadastrarAcessoUsuarioHoje",
					type:"POST",
					dataType:"JSON",
					data:{
						id_usuario:$("#id_user").val(),
						acao:2,
						conteudo:$("#inputPesquisa").val()
					}
				});					
			}else{
				$.ajax({
					url:"http://portservise.esy.es/portservise_webservice/AcessoModel/cadastrarAcessoHoje",
					type:"POST",
					dataType:"JSON",
					data:{
						acao:2,
						conteudo:$("#inputPesquisa").val()
					}
				});
			}
		});
	},
	more:function(){
		$("#desce").click(function(){
			var id_servico = $(this).attr("more");
			var url = "id="+id_servico+"&"+$("#filtro_anuncio").serialize();
			//$("#tbody_listAll").html("<center><img src='/templates/Back/img/input-spinner.gif' /></center>");
			$.ajax({
				url:"http://portservise.esy.es/portservise_webservice/ServicoModel/listServicosImagem",
				type:"POST",
				dataType:"JSON",
				data:url,
				success:function(data){
					//console.log(data);
					var tr = "";
					for(var i = 0; i < data.length; i++){
						var date = data[i].data_cadastro.split(" ");
						date = date[0].split("-").reverse().join("/");
						var imagem = "";
						try{
							imagem = "/web/uploadFile/"+data[i].fotos[0].imagem;
						}catch(e){
							imagem = "css/images/180x100.png";
						}
						var numero = "";
						try{
							numero = data[i].descricao;
							//console.log(numero.length);
							numero = (numero.length);
						}catch(e){
							
						}
						tr += '<div class="row  listing-row"><div class="col-sm-2"><a style="cursor:pointer;" class="thumbnail " ><img attr_id='+data[i].id_servico+' class="detalhes" alt="176 * 120" src="'+imagem+'"></a>';
						tr += '</div><div class="col-sm-10"><h3><a class="detalhes" attr_id='+data[i].id_servico+' style="cursor:pointer;">'+data[i].titulo+' - <strong>R$'+data[i].valor+'</strong></a></h3>';
						tr += '<p class="muted">Localizado em <strong>'+data[i].endereco+', '+data[i].cidade+' - cep:'+data[i].cep+'</strong></p><p class="muted">Postado em: '+date+' na Categoria <a style="cursor:pointer;" class="underline">'+data[i].categoria_descricao+'</a></p>';
						if(numero > 100){
							tr += '<p>'+(data[i].descricao.substring(0,(numero - 50)).replace(" ","\n")+"...")+'</p>';
						}else{
							tr += '<p>'+(data[i].descricao)+'</p>';
						}
						tr += '<p><span class="classified_links pull-right"><a class="link-info underline" style="cursor:pointer;">Compartilhe</a>&nbsp;<a class="link-info underline" style="cursor:pointer;">Adicione aos favoritos</a>';
						tr += '&nbsp;<a class="link-info underline detalhes" attr_id='+data[i].id_servico+' style="cursor:pointer;">Detalhes</a>&nbsp;&nbsp;<a class="link-info underline" style="cursor:pointer;">Contato</a></span></p></div></div>';
						tr += '<div id='+data[i].id_servico+' style="display:none;">'+JSON.stringify(data[i])+'</div>';
						$('#id_servico').val(data[i].id_servico);
						var id_more = data[i].id_servico;
						if(servico.lastIdList != id_more){
							$("#desce").css({"display":"block"}).attr("more",data[i].id_servico);
						}else{
							$("#desce").css({"display":"none"});
						}
						
					}
					$("#tbody_listAll").append(tr);
					servico.detalhe();
					servico.pesqisar();
				},
				error:function(data){
			
				}
			});
			servico.detalhe();			
		});
	},
	mapa:function(){
		$("#mapa").click(function(){
			$(this)
		});
	},
	contadorVisualiza:function(idAnuncio, idUsuario){
		
		$.ajax({
			url:"http://portservise.esy.es/portservise_webservice/AcessoModel/cadastrarAcessoUsuarioHoje",
			data:{
						id_usuario: idUsuario,
						acao: 5,
						conteudo: idAnuncio
					},
			type:"POST",
			dataType:"JSON",
			success:function(data){
			
			},
			error:function(data){
				console.log(data);
			}			
		});

	},
	filtro:function(){
		$("#bt_filtro").click(function(){
			servico.initList();
		});
	},
	initList:function(){
		$.ajax({
			url:"http://portservise.esy.es/portservise_webservice/ServicoModel/lastIdservico",
			type:"POST",
			dataType:"JSON",
			success:function(data){
				servico.listAll(data.id);
			}
		});
	},
	AjaxLastIdList:function(){
		$.ajax({
			url:"http://portservise.esy.es/portservise_webservice/ServicoModel/firstIdservico",
			type:"POST",
			dataType:"JSON",
			success:function(data){
				servico.lastIdList = data.id;
			}
		});		
	},
	lastIdList:1
	
}

servico.construct;