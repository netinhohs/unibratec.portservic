var contrato = {
	init : $(function(){
		contrato.mandarContrato();
	}),
	mandarContrato:function(){
		$("#enviar_contrato").click(function(){
			var dados = $("#mandar_contrato").serialize();
			$.ajax({
				url: "http://portservise.esy.es/portservise_webservice/ContratoModel/sendContrato",
				type: "POST",
				data: dados,
				dataType: "JSON",
				success: function(result){
					if(result.success){
						$.ajax({
							url: "http://portservise.esy.es/web/util/emailEnviarContrato.php",
							type: "POST",
							data: dados+"&nome="+result.nome+"&celular="+result.celular+"&telefone="+result.telefone+"&email="+result.email									
						});
						$("#enviar_contrato_close").trigger("click");
					}else{
						$("#error_contrato").css({"display":"block"});
						$("#error_contrato_span").html(result.error);
					}
				}
			});			
		});
	}
}

contrato.init;