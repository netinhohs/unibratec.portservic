var estados = {
	init : $(function(){
		estados.listAll();
		estados.clickUf();
	}), 
	listAll:function(){
		$.ajax({
			url:"http://portservise.esy.es/portservise_webservice/EstadosModel/listEstadosAll",
			type:"POST",
			dataType:"JSON",
			success:function(data){
				var indice = 0;
				var divmain = '';
				for(var i = 0; i < data.length; i++){
					if(indice == 0){
						divmain += '<div class="col-sm-3"><div class="row directory-block"><div class="col-sm-12">';
					}
					divmain += '<a class="uf" style="cursor:pointer;" attr="'+data[i].uf+'">'+data[i].nome+' ('+data[i].servicos+')</a><br />';
					indice++;
					if(indice == 12){
						divmain += '</div></div></div>';
						indice = 0;
					}
				}
				$("#BR").append(divmain);
				estados.clickUf();
			}
		});
	},
	
	clickUf:function(){
		$(".uf").click(function(){
			$("#uf").val($(this).attr("attr"));
			$("#bt_filtro").trigger("click");
		});
	}
	
}
estados.init;

	

