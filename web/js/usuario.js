var usuario = {
	construct:$(function(){
		usuario.cadastrar();
		usuario.login();
		usuario.userPerfil();
		usuario.mascaraTelefone();
		usuario.mascaraCpfCnpj();
		usuario.auterarSenha();
		usuario.esqueciSenha();
		usuario.buscarCepCorreios();
		
	}),
	cadastrar:function(){
		$("#cadastro_usuario").click(function(){
			if(
				$("#exampleInputPassword1").val() != "" &&
				$("#senha").val() != "" &&
				$("#email").val() != "" &&
				$("#nome").val() != "" 				
				){
				if($("#exampleInputPassword1").val() == $("#senha").val()){
					var serialize = $(".form-vertical").serialize();
					$.ajax({
						url:"http://portservise.esy.es/portservise_webservice/UsuarioModel/insertUsuario",
						data:serialize,
						type:"POST",
						dataType:"JSON",
						success:function(data){
							if(data.success){
								console.log(data.User);
								$.ajax({
									url:"/web/ajax.handle.page/ajax.create.user.php",
									data:{usuario:data.User},
									type:"POST",
									success:function(data){
										window.location = "perfil.php";
									},
									error:function(data){
										console.log(data);
										alert('erro')
									}
								});
							}else{
								$("#danger").show();
								if(data.error.indexOf('SQLSTATE[23000]') != -1){
									$("#span_alerta").html("E-mail já é adastrado");
								}
							}
						},
						error:function(data){
							console.log(data);
						}
					});
				}else{
					$("#danger").show();
					$("#span_alerta").html("Os campos senha e confirmaçao de senha estao diferentes");
					//alert("Os campos senha e confirmaçao de senha estao diferentes");
				}	
			}else{
				$("#danger").show();
				$("#span_alerta").html("Todos os campos devem ser preechidos");
				//alert("Todos os campos devem ser preechidos");
			}
		});
},
login:function(){
	$("#login_usuario").click(function(){
		if(
			$("#login_senha").val() != "" && 
			$("#login_email").val() != "" 
			){
			var serialize = $("#form_login_usuario").serialize();
		$.ajax({
			url:"http://portservise.esy.es/portservise_webservice/UsuarioModel/logar",
			data:serialize,
			type:"POST",
			dataType:"JSON",
			success:function(data){

				if(data.success){
					console.log(data.User);
					$.ajax({
						url:"ajax.handle.page/ajax.create.user.php",
						data:{usuario:data.User[0]},
						type:"POST",
						success:function(data){
							window.location ="dashboard.php";
						},
						error:function(data){
							console.log(data);
							alert('Erro')
						}
					});
				}else{
					$("#danger_login").show();
					$("#span_alerta_login").html("Usuario ou senha invalido.");
				}
			},
			error:function(data){
				console.log(data);
				alert('Erro logon')
			}
		});	
	}else{
		$("#danger_login").show();
		$("#span_alerta_login").html("Todos os campos devem ser preechidos");			
	}
});
},
endereco:function(user){
	var user = user.replace("id=", "usuario_id=") ;
	console.log(user);
	$.ajax({
		url:"http://portservise.esy.es/portservise_webservice/EnderecoModel/enderecoUsuarioById",
		data:user,
		type:"POST",
		success:function(data){
			$.ajax({
				url:"/web/ajax.handle.page/ajax.create.address.php",
				data:{endereco:data.Adress},
				type:"POST",
				error:function(data){
					console.log(data);
				}
			});
		},
		error:function(data){
			console.log(data);
		}			
	});
},
listEndereco:function(idUser){
		$.ajax({
			url:"http://portservise.esy.es/portservise_webservice/EnderecoModel/listEnderecosUsuarioById2",
			data:{id:idUser},
			type:"POST",
			dataType:"JSON",
			success:function(data){
				console.log(data);
				$("#uf").val(data[0].uf);
				$("#cep").val(data[0].cep);
				$("#endereco").val(data[0].endereco);
				$("#numero").val(data[0].numero);
				$("#bairro").val(data[0].bairro);
				$("#cidade").val(data[0].cidade);
				$.ajax({
					url:"/web/ajax.handle.page/ajax.create.address.php",
					data:{endereco:data[0]},
					type:"POST",
					error:function(data){
						console.log(data);
					}
				});				
			},
			error:function(data){
				console.log(data);
			}			
		});		
},
userPerfil:function(){
	$("#btPerfil").click(function(){
		var url = $("#prefil_usuario").serialize();
		$.ajax({
			url:"http://portservise.esy.es/portservise_webservice/UsuarioModel/updateUsuario",
			data:url,
			type:"POST",
			dataType:"JSON",
			success:function(data){
				if(data.success){
					console.log(data.usuario);
					usuario.endereco(url);
					$.ajax({
						url:"/web/ajax.handle.page/ajax.create.user.php",
						data:{usuario:data.usuario},
						type:"POST",
						success:function(data){
							window.location = "perfil.php";
						},
						error:function(data){
							console.log(data);
						}
					});
				}					
			},
			error:function(data){
				console.log(data);
			}			
		});	
	});
},
mascaraTelefone:function(){
	$("#telefone").keyup(function(){
		var v = $(this).val();
		v = v.replace(/\D/g,"");            
		v = v.replace(/^(d{2})(d)/g,"($1) $2"); 
		v = v.replace(/(d)(d{4})$/,"$1-$2");
		$(this).val(v);
	});
	$("#celular").keyup(function(){
		var v = $(this).val();
		v = v.replace(/\D/g,"");            
		v = v.replace(/^(\d{2})(\d)/g,"($1) $2"); 
		v = v.replace(/(\d)(\d{4})$/,"$1-$2");
		$(this).val(v);
	});
},
mascaraCpfCnpj:function(){
	$("#cpf_cnpj").keyup(function(){
		var v = $(this).val();
		v = v.replace(/\D/g,"");            
		$(this).val(v);
	});
},
uploadFotoperfil:function(id){
	var form;
	$('#js-upload-files').change(function (event) {
			//console.log(event.target.files[0]);
			var foto = event.target.files[0].name.split(".");
			$("#foto").val(foto[0]+"_"+id+"."+foto[1]);
			form = new FormData();
			form.append('foto', event.target.files[0]); // para apenas 1 arquivo
			form.append('id', id);
			//var name = event.target.files[0].content.name; // para capturar o nome do arquivo com sua extenção
			$.ajax({
				url: 'http://portservise.esy.es/portservise_webservice/UsuarioModel/inserirFotoPerfil', 
				data: form,
				dataType:"JSON",
				processData: false,
				contentType: false,
				type: 'POST',
				success: function (retorno) {
					console.log(retorno);
					if(retorno.success){
						$("#infosuccess").toggle();
						$("#infoSpansuccess").html("foto atualizada");
					}else{
						$("#infosuccess").toggle();
						$("#infoSpansuccess").html("foto atualizada");						
					}
				}
			});
		});	
},
auterarSenha:function(){
	$("#trocaSenha").click(function(){
		var senhaAtual = $('#senhaAtual').val();
		var NovaSenha = $('#novasenha').val();
		var RepetirMovaSenha = $('#RepetirMovaSenha').val();
		var form_troca_senha = $("#form_trocar_senha").serialize();
		$("#trocasenha_ok").hide();


		if (senhaAtual != "" && NovaSenha != "" && RepetirMovaSenha != "") {
			$("#danger_trocasenha").hide();
			if (NovaSenha == RepetirMovaSenha) {
				$("#danger_trocasenha").hide();
				var objUsuario;
				$.ajax({
					url:"http://portservise.esy.es/portservise_webservice/UsuarioModel/listUsuariosById",
					data:form_troca_senha,
					type:"POST",
					dataType:"JSON",
					success:function(data){

						console.log('Foi ' + data[0].senha);
						objUsuario = data[0];
						if (data[0].senha == senhaAtual) {
							objUsuario.senha = NovaSenha;
							$("#danger_trocasenha").hide();
							$.ajax({
								url:"http://portservise.esy.es/portservise_webservice/UsuarioModel/trocarSenha",
								data:form_troca_senha,
								type:"POST",
								dataType:"JSON",
								success:function(data){
									$("#trocasenha_ok").show();
									$('#senhaAtual').val('');
									$('#novasenha').val('');
									$('#RepetirMovaSenha').val('');
									$.ajax({
										url:"/web/ajax.handle.page/ajax.create.user.php",
										data:{usuario:objUsuario},
										type:"POST",
										success:function(data){
											console.log(data);
										},
										error:function(data){
											console.log(data);
										}
									});
								},
								error:function(data){
									console.log(data);
								}			
							});
						}else{
							$("#danger_trocasenha").show();
							$("#span_alerta_trocarSenha").html("Senha atual diferenta da informada");
						};	
					}, 
					error:function(data){
						console.log('Erro');
					}			
				});	


			}else{
				$("#danger_trocasenha").show();
				$("#span_alerta_trocarSenha").html("A nova senha esta diferenco da repetir senha.");
			};

		}else{
			$("#danger_trocasenha").show();
			$("#span_alerta_trocarSenha").html("Todos os campos devem ser preenchido.");
		};

	});
},
esqueciSenha:function(){
	$("#esquiciSenhaNova").click(function(){
		
		var emailUser = $('#emailEsqNova').val();
		var senhaNova = $('#senhaEsqNova').val();
		var senhaNovaRep = $('#senhaEsqNovaRep').val();
		var idUser = $('#idUser').val();
		var objUsuario; 



		if (emailUser != "" && senhaNova != "" && senhaNovaRep != "" && idUser!= "") {
			$("#danger_nv_senha").hide();
			if (senhaNova == senhaNovaRep) {
			$("#danger_nv_senha").hide();
			$.ajax({
				url:"http://portservise.esy.es/portservise_webservice/UsuarioModel/listUsuariosById",
				data:{id:idUser},
				type:"POST",
				dataType:"JSON",
				success:function(data){  
				objUsuario = data[0];         
					try {
						$("#danger_esquecisenha").hide();
						$("#esqueci_ok").show();
						if(data[0].email == emailUser){
							$.ajax({
								url:"http://portservise.esy.es/portservise_webservice/UsuarioModel/trocarSenha",
								data:{id:idUser, novaSenha:senhaNovaRep},
								type:"POST",
								dataType:"JSON",
								success:function(data){                                      

									try {
										$.ajax({
										url:"ajax.handle.page/ajax.create.user.php",
										data:{usuario:objUsuario},
										type:"POST",
										success:function(data){
											console.log(data);
										},
										error:function(data){
											console.log(data);
										}
									});
										$("#esqueciNova_ok").show();

										setTimeout(function(){ window.location ="dashboard.php"; }, 3000);
									} catch(e) {
                                        $("#danger_nv_senha").show();
										$("#esqueci_ok").hide();
										$("#span_vn_senha").html("Não foi possivel trocar sua senha. Tente novamente mas tarde.");
                                    }

                                },
                                error:function(data){
                                	console.log('e');
                                }           
                            });

						}else{
							$("#danger_nv_senha").show();
							$("#esqueciNova_ok").hide();
							$("#span_vn_senha").html("Esse email não existe.");
						}
					} catch(e) {
                        // statements
                        $("#danger_nv_senha").show();
                        $("#esqueciNova_ok").hide();
                        $("#span_vn_senha").html("Esse email não existe.");
                    }

                },
                error:function(data){
                	console.log('e');
                }           
            });
		}else{
			$("#danger_nv_senha").show();
		    $("#span_vn_senha").html("A nova senha esta diferenco da repetir senha.");
		}
	}else{
		$("#danger_nv_senha").show();
		$("#span_vn_senha").html("Todos os campos devem ser preenchido.");	
	}


	});
},
buscarCepCorreios:function(){
		$("#cep").blur(function(){
			if($(this).val() != ""){
				$.ajax({
					url:"http://clareslab.com.br/ws/cep/json/"+$(this).val()+"/",
					type:"POST",
					dataType:"JSON",
					success:function(data){
						$("#cidade").val(data.cidade);
						$("#uf").val(data.uf);
						$("#endereco").val(data.endereco);
						$("#bairro").val(data.bairro);
						console.log(data);
					},
					error:function(data){

					}
				});	
			}
		});	
	}

}

usuario.construct;