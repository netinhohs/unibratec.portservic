<?php include 'head.php'; ?>
<?php include 'util/util.php'; ?>

<?
session_start();
if(isset($_POST['json'])){
    $_SESSION['details'] = json_decode($_POST['json'],true);
}
 //echo "<pre>";
 //print_r($_SESSION['endereco']);
   //              print_r($_SESSION['details']);
 //echo "</pre>";
$ip = getIp();
?>
<body>
    <?php include 'nav_top.php';    ?>
    <div class="jumbotron home-tron-search well ">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="home-tron-search-inner">
                        <div class="row">
                            <div class="col-sm-8 col-xs-9" style="text-align: center">
                                <div class="row">
                                    <div class="col-sm-12 col-sm-offset-1">
                                        <div class="input-group">
                                            <span class="input-group-addon input-group-addon-text hidden-xs">Encontre-me um</span>
                                            <input id="inputPesquisa" name="inputPesquisa" type="text" class="form-control col-sm-3" placeholder="Pedreiro, Chaveiro, Mecanico, Pintor ">
                                            <div class=" input-group-addon hidden-xs">
                                                <div class="btn-group" >
                                                    <button type="button" class="btn  dropdown-toggle" data-toggle="dropdown">
                                                        Categorias <span class="caret"></span>
                                                    </button>
                                                    <ul id="dropdown-menu" class="dropdown-menu" role="menu">
                                                        <!-- <li><a href="#">Cars, Vans & Motorbikes</a></li>
                                                        <li><a href="#">Community</a></li>
                                                        <li><a href="#">Flats & Houses</a></li>
                                                        <li><a href="#">For Sale</a></li>
                                                        <li><a href="#">Jobs</a></li>
                                                        <li><a href="#">Pets</a></li>
                                                        <li><a href="#">Services</a></li> -->
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-xs-3" style="text-align: center">
                                <div class="row">
                                    <div class="col-sm-11 pull-right">
                                        <button class="btn btn-primary search-btn"><i class="icon-search"></i>&nbsp;&nbsp;Pesquise</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="container" id="listings-page">
        <div class="row">
            <div class="col-sm-12 listing-wrapper listings-top listings-bottom">
                <br />
                <br />
                
                <div class="row">
                    <div class="col-sm-7">
                        <ol class="breadcrumb">
                            <li><a style="cursor:pointer;" onclick="window.history.back();" class="link-info"><i class="fa fa-chevron-left"></i> Voltar a Lista</a></li>
                            <li><a id="menu_link_categoria" href="#"></a></li>
                            <li class="active"><?= $_SESSION['details']['categoria_descricao'];?>
                            </ol>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-7">
                            <h1><?= $_SESSION['details']['titulo'];?></h1>
                            <p>Localização: <?= $_SESSION['details']['cidade'];?>, <?= $_SESSION['details']['uf'];?></p>
                        </div>
                        <div class="col-sm-5">
                            <p class="price">R$<?= str_replace(".",",",$_SESSION['details']['valor']);?></p>
                        </div>
                    </div>          <div class="row">
                    <div class="col-sm-7">
                        <div class="row">
                            <div class="col-xs-3" style="width: 100px;">
                                <!-- Place this tag where you want the share button to render. -->
                                <div class="g-plus" data-action="share" data-annotation="bubble"></div>
                                <!-- Place this tag after the last share tag. -->
                                <script type="text/javascript">
                                (function() {
                                    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                                    po.src = '../../apis.google.com/js/platform.js';
                                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                                })();
                                </script>
                            </div>
                            <div class="col-xs-3" style="width: 100px;">
                                <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
                                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                            </div>
                            <div class="col-xs-3" style="width: 100px;">
                                <div class="fb-share-button" data-href="http://developers.facebook.com/docs/plugins/" data-type="button_count"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr />
                <div class="row">
                    <div class="col-sm-7">
                        <h3>Descrição</h3>
                        <!--<div class="row">
                            <div class="col-xs-6">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th>Make</th>
                                            <td>Porsche</td>
                                        </tr>
                                        <tr>
                                            <th>Model</th>
                                            <td>Boxster</td>
                                        </tr>
                                        <tr>
                                            <th>Mileage</th>
                                            <td>80,000 Kms</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-xs-6">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th>Year</th>
                                            <td>2007</td>
                                        </tr>
                                        <tr>
                                            <th>Type of car:</th>
                                            <td>4-door</td>
                                        </tr>
                                        <tr>
                                            <th>Body type</th>
                                            <td>Estate</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>-->
                        <p>
                            <strong>
                                <?= $_SESSION['details']['descricao'];?>
                            </strong>
                        </p>
                        <p>
                            <span class="classified_links ">
                                <a class="link-info" href="#"><i class="fa fa-share"></i> Compartilhe</a>&nbsp;
                                <a class="link-info " href="#"><i class="fa fa-star"></i> Adicione aos faveoritos</a>
								<?
								if(isset($_SESSION['usuario'])){
								?>
									&nbsp;<a class="link-info " href="#"><i class="fa fa-envelope-o"></i> Contato</a>
									&nbsp;<a class="link-info fancybox-media" href="http://maps.google.com/?ll=-8.0464433,-35.0025288&amp;spn=0.003833,0.010568&amp;t=h&amp;z=17"><i class="fa fa-map-marker"></i> Mapa</a></span>
								<?
								}
								?>
                            </p>
                        </div>
                        <div class="col-sm-5 center zoom-gallery">
                            <div class="row center">
                                <div class="col-sm-12">
                                    <?
                                    $img = "";
                                    if(count($_SESSION['details']['fotos']) > 0){
                                        $img = "/web/uploadFile/".$_SESSION['details']['fotos'][0]["imagem"];
                                    }else{
                                        $img = "css/images/180x100.png";
                                    }
                                    ?>
                                    <a class="fancybox" rel="group" href="<?= $img;?>">
                                        <img id="img_01" alt="" class="raised" src="<?= $img;?>" style="width: 100%" />
                                    </a>
                                    <br />
                                    <br />
                                    <div class="row" id="gallery" >
                                        <?
                                        for($i = 0; $i < count($_SESSION['details']['fotos']); $i++){
                                            ?>
                                            <div class="col-xs-4" style="margin-bottom: 10px;">
                                                <a href="/web/uploadFile/<?= $_SESSION['details']['fotos'][$i]["imagem"]; ?>" class="fancybox thumbnail" rel="group" >
                                                    <img alt="" src="/web/uploadFile/<?= $_SESSION['details']['fotos'][$i]["imagem"]; ?>" style="width: 100%" />
                                                </a>
                                            </div>
                                            <?
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <br />
                            <div class="col-sm-12" style="text-align: center; margin: 0 auto">
                                <?
									if(isset($_SESSION['usuario'])){
										echo '<button data-toggle="modal" data-target="#myModal" class="btn btn-warning" style="text-align: center;width: 180px; " type="button">Responder</button>';
									}
								?>   
                                <br />
                                <p></p>
                            </div>
                            <br />
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">Perguntas ao vendedor</div>
                        <div class="panel-body">
                            <? //if(isset($_SESSION['usuario'])){ ?>
                            <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#fazerPergunta">
                                Faça uma pergunta ao prestador
                            </button>
                            <hr>
                            <? //} ?>
                            
                            
                            
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <p><i class="fa fa-comment" aria-hidden="true"></i>
                                        Quando tempo para terminar
                                    </p> &nbsp;&nbsp;&nbsp;&nbsp;
                                    <i class="fa fa-comments-o" aria-hidden="true"></i></i>
                                    Entrono de 10 a 30 dias
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <p><i class="fa fa-comment" aria-hidden="true"></i>
                                        Quando tempo para terminar
                                    </p> &nbsp;&nbsp;&nbsp;&nbsp;
                                    <i class="fa fa-comments-o" aria-hidden="true"></i></i>
                                    Entrono de 10 a 30 dias
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 listings">
                        <br />
                        <div class="row">
                            <div class="panel panel-default recent-listings hidden-xs">
                                <div class="panel-heading">Recent Cars, Vans & Motorbikes ads in London</div>
                                <div class="panel-body">
                                    <div class="row premium  listing-row">
                                        <div class="col-sm-2">
                                            <a href="details.html" class="thumbnail " ><img alt="" src="css/images/listings/0.jpg"></a>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="row">
                                                <div class="col-sm-9">
                                                    <h3><a class=""  href="details.html">7 bedroom house for sale</a></h3>
                                                </div>
                                                <div class="col-sm-3">
                                                    <h3 class="price-text"><strong>&pound;3,350,000</strong></h3>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <p class="muted">Located in <strong>Richmond, London</strong></p>
                                                    <p class="muted">Posted Feb 05, 2014 to <a href="#" class="underline">Cars, Vans & Motorbikes</a></p><br />
                                                    <p>Excellent Condition. Displacement: 2,900 cc Power Output: 250 bhp Drivetrain: 2-WD  Second hand car for sale – 7377790212 Available car – beat – blue color, Honda city –white /black Indica ecs – white Alto – white Nano – red /white...</p><br />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-sm-2"></div>
                                                <div class="col-sm-5">
                                                    <p class="ad-description">
                                                        <strong>2006</strong> |
                                                        <strong>98,000 miles</strong> |
                                                        <strong>2,696 cc</strong> |
                                                        <strong>Diesel</strong>
                                                    </p>
                                                </div>
                                                <div class="col-sm-5">
                                                    <p>
                                                        <span class="classified_links pull-right">
                                                            <a class="link-info underline" href="#">Share</a>&nbsp;
                                                            <a class="link-info underline" href="#">Add to favorites</a>
                                                            &nbsp;<a class="link-info underline" href="#">Contact</a></span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row   listing-row">
                                            <div class="col-sm-2">
                                                <a href="details.html" class="thumbnail " ><img alt="" src="css/images/listings/1.jpg"></a>
                                            </div>
                                            <div class="col-sm-10">
                                                <div class="row">
                                                    <div class="col-sm-9">
                                                        <h3><a class=""  href="details.html">1 bedroom flat for sale</a></h3>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <h3 class="price-text"><strong>&pound;229,950</strong></h3>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <p class="muted">Located in <strong>Richmond, London</strong></p>
                                                        <p class="muted">Posted Feb 05, 2014 to <a href="#" class="underline">Cars, Vans & Motorbikes</a></p><br />
                                                        <p>Excellent Condition. Displacement: 2,900 cc Power Output: 250 bhp Drivetrain: 2-WD  Second hand car for sale – 7377790212 Available car – beat – blue color, Honda city –white /black Indica ecs – white Alto – white Nano – red /white...</p><br />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class="col-sm-2"></div>
                                                    <div class="col-sm-5">
                                                        <p class="ad-description">
                                                            <strong>2006</strong> |
                                                            <strong>98,000 miles</strong> |
                                                            <strong>2,696 cc</strong> |
                                                            <strong>Diesel</strong>
                                                        </p>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <p>
                                                            <span class="classified_links pull-right">
                                                                <a class="link-info underline" href="#">Share</a>&nbsp;
                                                                <a class="link-info underline" href="#">Add to favorites</a>
                                                                &nbsp;<a class="link-info underline" href="#">Contact</a></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br />
                                            <div class="row   listing-row">
                                                <div class="col-sm-2">
                                                    <a href="details.html" class="thumbnail property_sold"  style="position:relative;"><img alt="" src="css/images/listings/2.jpg"></a>
                                                </div>
                                                <div class="col-sm-10">
                                                    <div class="row">
                                                        <div class="col-sm-9">
                                                            <h3><a class=""  href="details.html">2 bedroom apartment for sale</a></h3>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <h3 class="price-text"><strong>&pound;520,000</strong></h3>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <p class="muted">Located in <strong>Richmond, London</strong></p>
                                                            <p class="muted">Posted Feb 05, 2014 to <a href="#" class="underline">Cars, Vans & Motorbikes</a></p><br />
                                                            <p>Excellent Condition. Displacement: 2,900 cc Power Output: 250 bhp Drivetrain: 2-WD  Second hand car for sale – 7377790212 Available car – beat – blue color, Honda city –white /black Indica ecs – white Alto – white Nano – red /white...</p><br />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="row">
                                                        <div class="col-sm-2"></div>
                                                        <div class="col-sm-5">
                                                            <p class="ad-description">
                                                                <strong>2006</strong> |
                                                                <strong>98,000 miles</strong> |
                                                                <strong>2,696 cc</strong> |
                                                                <strong>Diesel</strong>
                                                            </p>
                                                        </div>
                                                        <div class="col-sm-5">
                                                            <p>
                                                                <span class="classified_links pull-right">
                                                                    <a class="link-info underline" href="#">Share</a>&nbsp;
                                                                    <a class="link-info underline" href="#">Add to favorites</a>
                                                                    &nbsp;<a class="link-info underline" href="#">Contact</a></span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br />
                                                <div class="row  last listing-row">
                                                    <div class="col-sm-2">
                                                        <a href="details.html" class="thumbnail " ><img alt="" src="css/images/listings/3.jpg"></a>
                                                    </div>
                                                    <div class="col-sm-10">
                                                        <div class="row">
                                                            <div class="col-sm-9">
                                                                <h3><a class=""  href="details.html">2 bedroom house for sale</a></h3>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <h3 class="price-text"><strong>&pound;550,000</strong></h3>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <p class="muted">Located in <strong>Richmond, London</strong></p>
                                                                <p class="muted">Posted Feb 05, 2014 to <a href="#" class="underline">Cars, Vans & Motorbikes</a></p><br />
                                                                <p>Excellent Condition. Displacement: 2,900 cc Power Output: 250 bhp Drivetrain: 2-WD  Second hand car for sale – 7377790212 Available car – beat – blue color, Honda city –white /black Indica ecs – white Alto – white Nano – red /white...</p><br />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="row">
                                                            <div class="col-sm-2"></div>
                                                            <div class="col-sm-5">
                                                                <p class="ad-description">
                                                                    <strong>2006</strong> |
                                                                    <strong>98,000 miles</strong> |
                                                                    <strong>2,696 cc</strong> |
                                                                    <strong>Diesel</strong>
                                                                </p>
                                                            </div>
                                                            <div class="col-sm-5">
                                                                <p>
                                                                    <span class="classified_links pull-right">
                                                                        <a class="link-info underline" href="#">Share</a>&nbsp;
                                                                        <a class="link-info underline" href="#">Add to favorites</a>
                                                                        &nbsp;<a class="link-info underline" href="#">Contact</a></span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="fazerPergunta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">PERGUNTE AO PRESTADOR</h4>
                                        <span class="hidden-xs" style="font-size: 10px;">Não digite dados de contato, não use linguagem vulgar, oferte ou pergunte por outro produto.</span>
                                    </div>
                                    <div class="modal-body">
                                        <div id="msg_resposta"></div>
                                        <div id="msg_enviada">
                                            <form id="mandar_pergunta" class="form-vertical">
                                                <fieldset>
                                                    <div class="row">
                                                        <div class="col-sm-12" >
                                                            <!-- Textarea -->
                                                            <div class="form-group">
                                                                <label class="col-md-10 control-label" for="textarea"></label>
                                                                <div class="col-md-12">
                                                                    <textarea class="form-control" id="pergunta" name="pergunta" rows="6"placeholder="Escreva sua mensagem..."></textarea>
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="id_usuario"  value="<?= $_SESSION['usuario']['id']; ?>"/>
                                                            <input type="hidden" name="id_servico"  value="<?= $_SESSION['details']['id']; ?>" />
                                                            <input type="hidden" name="id_prestador" id="prestador_id" value="<?= $_SESSION['details']['usuario_id']; ?>" />
                                                            <input type="hidden" name="descricao" id="descricao_prest" value="<?= $_SESSION['details']['descricao']; ?>" />
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button id="fecharPergunta"  type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                            <button id="enviar_pergunta"  type="button" class="btn btn-primary">Enviar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">Reply to Alan</h4>
                                        <p class="hidden-xs">Deseja realizar a contratação do serviço</p>
                                    </div>
                                    <div class="modal-body">
                                        <form id="mandar_contrato" class="form-vertical">
                                            <fieldset>
                                                <div class="row">
                                                    <div class="col-sm-12" >
                                                        <?
                                                        if(isset($_SESSION['usuario'])){
                                                            ?>
                                                            <input type="hidden" name="cliente_id" id="cliente_id" value="<?= $_SESSION['usuario']['id']; ?>"/>
                                                            <input type="hidden" name="prestador_id" id="prestador_id" value="<?= $_SESSION['details']['usuario_id']; ?>" />
                                                            <input type="hidden" name="cep" id="cep" value="<?= $_SESSION['details']['cep']; ?>" />
                                                            <input type="hidden" name="cidade" id="cidade" value="<?= $_SESSION['details']['cidade']; ?>" />
                                                            <input type="hidden" name="uf" id="uf" value="<?= $_SESSION['details']['uf']; ?>" />
                                                            <input type="hidden" name="bairro" id="bairro" value="<?= $_SESSION['details']['bairro']; ?>" />
                                                            <input type="hidden" name="servico_id" id="servico_id" value="<?= $_SESSION['details']['id']; ?>" />
                                                            <?
                                                        }
                                                        ?>
														
														<div class="alert alert-danger" style="display:none;" id="error_contrato">
														  <strong>Aviso!</strong> 
														  <span id="error_contrato_span">
														  </span>;
														</div>
                                                        <div class="alert alert-info hidden-xs">
                                                            <p>
                                                                Para realizar a contratação é necessario estar com todos os dados preenchedos:<br />
															<?
																$flagSed = 0;
																if(isset($_SESSION['endereco']['cep']) and $_SESSION['endereco']['cep'] == ""){
																	$flagSed = 1;
                                                            ?>                                                                
																1. CEP.<br /> 
															<?
																}
																if(isset($_SESSION['endereco']['cidade']) and $_SESSION['endereco']['cidade'] == ""){
																	$flagSed = 1;
															?>	
																2. CIDADE. <br />
															<?															
																}
																if(isset($_SESSION['endereco']['bairro']) and $_SESSION['endereco']['bairro'] == ""){
																	$flagSed = 1;
															?>		
																3. BAIRRO.<br />
															<?															
																}
																if(isset($_SESSION['endereco']['uf']) and $_SESSION['endereco']['uf'] == ""){
																	$flagSed = 1;
															?>		
																4. ESTADO.<br />
															<?															
																}
																if(isset($_SESSION['usuario']['celular']) and $_SESSION['usuario']['celular'] == ""){
																	$flagSed = 1;
															?>		
																5. CELULAR.<br />
															<?															
																}
																if(isset($_SESSION['usuario']['telefone']) and $_SESSION['usuario']['telefone'] == ""){
																	$flagSed = 1;
															?>																		
																6. TELEFONE.<br />
															<?															
																}
															?>																		
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button id="enviar_contrato_close"  type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
										<?
											if($flagSed == 0){
										?>										
										<button id="enviar_contrato"  type="button" class="btn btn-primary">Contratar</button>
										<?
											}
										?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script src="js/servico.js"></script>
                        <script src="js/categoria.js"></script>
                        <script src="js/contrato.js"></script>
                        <script src="js/perguntas.js"></script>
                        <script>
                        $(document).ready(function() {
                            servico.contadorVisualiza(<?= $_SESSION['details']['id'];?>, <?= $_SESSION['details']['usuario_id'];?>);
                        });
                        categoria.detalheCategoria(<?= $_SESSION['details']['categoria_self_join'];?>);
                        </script>
                        <?php include 'footer.php'; ?>
                    </body>
                    <!-- Mirrored from templates.expresspixel.com/bootlistings/details.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Feb 2016 12:17:46 GMT -->
                    </html>