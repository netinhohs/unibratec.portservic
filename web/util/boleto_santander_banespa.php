<?php
// +----------------------------------------------------------------------+
// | BoletoPhp - Vers�o Beta                                              |
// +----------------------------------------------------------------------+
// | Este arquivo est� dispon�vel sob a Licen�a GPL dispon�vel pela Web   |
// | em http://pt.wikipedia.org/wiki/GNU_General_Public_License           |
// | Voc� deve ter recebido uma c�pia da GNU Public License junto com     |
// | esse pacote; se n�o, escreva para:                                   |
// |                                                                      |
// | Free Software Foundation, Inc.                                       |
// | 59 Temple Place - Suite 330                                          |
// | Boston, MA 02111-1307, USA.                                          |
// +----------------------------------------------------------------------+

// +----------------------------------------------------------------------+
// | Originado do Projeto BBBoletoFree que tiveram colabora��es de Daniel |
// | William Schultz e Leandro Maniezo que por sua vez foi derivado do	  |
// | PHPBoleto de Jo�o Prado Maia e Pablo Martins F. Costa                |
// |                                                                      |
// | Se vc quer colaborar, nos ajude a desenvolver p/ os demais bancos :-)|
// | Acesse o site do Projeto BoletoPhp: www.boletophp.com.br             |
// +----------------------------------------------------------------------+

// +----------------------------------------------------------------------------+
// | Equipe Coordena��o Projeto BoletoPhp: <boletophp@boletophp.com.br>         |
// | Desenvolvimento Boleto Santander-Banespa : Fabio R. Lenharo                |
// +----------------------------------------------------------------------------+


// ------------------------- DADOS DIN�MICOS DO SEU CLIENTE PARA A GERA��O DO BOLETO (FIXO OU VIA GET) -------------------- //
// Os valores abaixo podem ser colocados manualmente ou ajustados p/ formul�rio c/ POST, GET ou de BD (MySql,Postgre,etc)	//

 $request = array('id' => $_GET['codusuario']);

    
$ch = curl_init('http://portservise.esy.es/portservise_webservice/UsuarioModel/listUsuariosById');

curl_setopt($ch, CURLOPT_POST, true);                                                                    
curl_setopt($ch, CURLOPT_POSTFIELDS, $request);                                                                  
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      

$result = json_decode(curl_exec($ch), true);
curl_close($ch);

foreach ($result as $arrusuario => $usuarios) {
    $usuario = $usuarios;
}


$ch = curl_init('http://portservise.esy.es/portservise_webservice/UsuarioModel/enderecoById');

curl_setopt($ch, CURLOPT_POST, true);                                                                    
curl_setopt($ch, CURLOPT_POSTFIELDS, $request);                                                                  
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      

$result = json_decode(curl_exec($ch), true);
curl_close($ch);

foreach ($result as $arrendereco => $enderecos) {
    $endereco = $enderecos;
}

	if(count($usuario) > 0){
		// DADOS DO BOLETO PARA O SEU CLIENTE
		$dias_de_prazo_para_pagamento = 7;
		$taxa_boleto = 2.95;
		$data_venc = date("d/m/Y", time() + ($dias_de_prazo_para_pagamento * 86400));  // Prazo de X dias OU informe data: "13/04/2006"; 
		$valor_cobrado = $_GET['valor']; // Valor - REGRA: Sem pontos na milhar e tanto faz com "." ou "," ou com 1 ou 2 ou sem casa decimal
		$valor_cobrado = str_replace(",", ".",$_GET['valor']);
		$valor_boleto=number_format($valor_cobrado+$taxa_boleto, 2, ',', '');

		$dadosboleto["nosso_numero"] = "1234567";  // Nosso numero sem o DV - REGRA: M�ximo de 7 caracteres!
		$dadosboleto["numero_documento"] = date('is');	// Num do pedido ou nosso numero
		$dadosboleto["data_vencimento"] = $data_venc; // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
		$dadosboleto["data_documento"] = date("d/m/Y"); // Data de emiss�o do Boleto
		$dadosboleto["data_processamento"] = date("d/m/Y"); // Data de processamento do boleto (opcional)
		$dadosboleto["valor_boleto"] = $valor_boleto; 	// Valor do Boleto - REGRA: Com v�rgula e sempre com duas casas depois da virgula

		// DADOS DO SEU CLIENTE
		$dadosboleto["sacado"] = $usuario['nome'];
		$dadosboleto["endereco1"] = $endereco['endereco'];
		$dadosboleto["endereco2"] = $endereco['cidade']. " - " . $endereco['uf'] . " -  " . $endereco['cep'];

		// INFORMACOES PARA O CLIENTE
		$dadosboleto["demonstrativo1"] = "Pagamento Relativo a Plano";
		//$dadosboleto["demonstrativo2"] = "Mensalidade referente a nonon nonooon nononon<br>Taxa banc�ria - R$ ".number_format($taxa_boleto, 2, ',', '');
		$dadosboleto["demonstrativo3"] = "Duvidas, acesse portservice.com.br";
		$dadosboleto["instrucoes1"] = "- Sr. Caixa, cobrar multa de 2% ap�s o vencimento";
		$dadosboleto["instrucoes2"] = "- Receber at� 7 dias ap�s o vencimento";
		//$dadosboleto["instrucoes3"] = "- Em caso de d�vidas entre em contato conosco: xxxx@xxxx.com.br";
		//$dadosboleto["instrucoes4"] = "&nbsp; Emitido pelo sistema Projeto BoletoPhp - www.boletophp.com.br";

		// DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
		$dadosboleto["quantidade"] = "";
		$dadosboleto["valor_unitario"] = "";
		$dadosboleto["aceite"] = "";		
		$dadosboleto["especie"] = "R$";
		$dadosboleto["especie_doc"] = "";


		// ---------------------- DADOS FIXOS DE CONFIGURA��O DO SEU BOLETO --------------- //


		// DADOS PERSONALIZADOS - SANTANDER BANESPA
		$dadosboleto["codigo_cliente"] = $usuario['id']; // C�digo do Cliente (PSK) (Somente 7 digitos)
		$dadosboleto["ponto_venda"] = "1333"; // Ponto de Venda = Agencia
		$dadosboleto["carteira"] = "102";  // Cobran�a Simples - SEM Registro
		$dadosboleto["carteira_descricao"] = "COBRAN�A SIMPLES - CSR";  // Descri��o da Carteira

		// SEUS DADOS
		$dadosboleto["identificacao"] = "PortService!";
		$dadosboleto["cpf_cnpj"] = "123.165.789-72";
		$dadosboleto["endereco"] = "Av. Recife 500 - CEP 50720-794";
		$dadosboleto["cidade_uf"] = "Recife / PE";
		$dadosboleto["cedente"] = "PortService Servicos de qualidade";

		// N�O ALTERAR!
		include("include/funcoes_santander_banespa.php"); 
		include("include/layout_santander_banespa.php");
	}else{
	    echo "Ocorreu um erro inesperado, favor entrar em contato com a administra��o";
	}
?>
