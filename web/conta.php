<?php 
    include 'verificar.php';
    include 'head.php'; ?>
<body>
    <?php include 'nav_top.php'; ?>
    <hr class="topbar"/>
    <div class="container">
        <div class="row">
            <?php include "menu-dashboard.php" ?>
            <div class="col-sm-9">
                <div class="panel panel-default">

                    <div class="panel-heading">Alterar senha</div>
                    <div class="panel-body">
                        <br /><div class="row">
                        <div class="alert alert-danger" id="danger_trocasenha" style="display:none;">
                            <strong>Alerta!</strong>
                            <span id="span_alerta_trocarSenha"></span>
                        </div>

                        <div class="alert alert-info" id="trocasenha_ok" style="display:none;">
                            <p>Senha alterada com sucesso.</p>
                        </div>

                    </div><br />
                    <form class="form-vertical" id="form_trocar_senha">
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-12 "  >
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Senha atual</label>
                                        <input type="password" class="form-control " id="senhaAtual" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Nova senha</label>
                                        <input type="password" class="form-control" id="novasenha"  name="novaSenha" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Repetir nova senha</label>
                                        <input type="password" class="form-control" id="RepetirMovaSenha" placeholder="">
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> Podemos contactá-lo com propriedades relevantes, ofertas e novidades
                                        </label>
                                    </div>
                                    <br />
                                    <input type="hidden" name="id" value="<?php echo $_SESSION['usuario']['id'];?>">
                                    <button type="button" class="btn btn-primary" id="trocaSenha">Salvar</button>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
    <br />
</div>
</div>
<script src="js/usuario.js"></script>

<?php include 'footer.php'; ?>
</body>
<!-- Mirrored from templates.expresspixel.com/bootlistings/account_account.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Feb 2016 12:17:51 GMT -->
</html>