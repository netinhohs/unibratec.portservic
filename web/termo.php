<?php include 'head.php'; ?>
<body>
    <?php include 'nav_top.php';?>
    <hr class="topbar"/>
    <div class="container">
        <br />
        <div class="row">
            <div class="col-sm-12">
                <h1>Termos e Condições de Uso e Acesso</h1>
                <hr />
                <h3>CONSIDERAÇÕES INICIAIS</h3>
                <ol>
                    <li>(a) PORTSERVICE é um site de serviços hospedado sob
                        o domínio portservice.com.br, e disponibilizado através
                    da URL www.portservice.com.br.</li> <br>
                    <li>(b) O serviço, oferecido pelo site portservice.com.br,
                        consiste na disponibilização de um espaço publicitário
                        para anunciantes prestadores de serviços ("Usuário(s)").
                        Portanto atua hospedando conteúdo de terceiros, ou seja,
                        anúncios de prestação de serviços, postados por eles
                    ("Usuário(s)").</li> <br>
                    <li>(c) Desta forma, o Usuário publica um anúncio de
                        prestação de serviços no site www.portservice.com.br,
                        com todos os detalhes possíveis, inclusive seus dados
                        pessoais para contato direto, negociação e conclusão da
                        transação, todos eles ocorridos fora do ambiente do site
                    portservice.com.br.</li> <br>
                    <li>(d) O Usuário que utiliza o site www.portservice.com.br
                        para vender ou comprar serviços, ou ainda encontrar as
                        informações que procura, o faz por sua conta e risco,
                        sem que PORT SERVICE seja responsável pela
                        veracidade dos dados especificados pelos Usuários, nem
                        pela qualidade e demais detalhes daquilo que é ofertado.
                        PORT SERVICE  não participa e/ou interfere, de qualquer
                        forma, da negociação e/ou efetivação de quaisquer
                    transações.</li> <br>
                </ol>
                1. <b>ACEITE</b> <br><br>
                <ul>
                    <li>1.1. PORT SERVICE oferece ao Usuário um conjunto de
                        recursos online (“Serviços”) para o uso da plataforma de
                        classificado virtual de prestadores de serviços, regido por
                        este Termos e Condições de Uso e Acesso (“TCUA”) da
                    plataforma do site www.portservice.com.br.</li>
                    <li>1.2. A utilização dos Serviços implica na mais alta
                        compreensão, aceitação e vinculação automática do
                        Usuário aos TCUA. Ao fazer uso de quaisquer dos
                        Serviços oferecidos, o Usuário concorda em respeitar e
                        seguir todas e quaisquer diretrizes dispostas nestes
                    TCUA.</li>
                    <li> 1.3. Caso tenha alguma objeção a quaisquer itens,
                        diretrizes ou alterações posteriores destes TCUA, ou,
                        ainda, caso fique insatisfeito com os Serviços oferecidos
                        pela PORT SERVICE, por qualquer motivo que seja, o
                    Usuário poderá cessar o uso imediatamente, sem ônus.</li>
                    <li> 1.4. Estes TCUA poderão sofrer alterações periódicas,
                        seja por questões legais ou estratégicas da PORT
                        SERVICE. O Usuário desde já concorda e reconhece que
                        é de sua única e inteira responsabilidade a verificação
                        periódica destes TCUA.  PORT SERVICE poderá, por
                        mera liberalidade, informar ao Usuário sobre alterações
                        significativas nos TCUA, através de avisos na página
                    principal de seu site.</li>
                </ul> <br>
                2. <b>POLÍTICA DE USO</b> <br><br>
                <ul>
                    <li>2.1. O Usuário está ciente de que todos os anúncios
                        disponíveis no site são produzidos e disponibilizados por
                        terceiros, passando primeiramente por controle,
                        monitoramento e verificação prévia dos administradores
                    da PORT SERVICE, </li>
                    <ul>
                        <li>2.1.1. O Usuário concorda que deve avaliar e assumir
                            quaisquer riscos associados a qualquer Conteúdo e/ou
                            Anúncio submetido ao site. Sob nenhuma circunstância,
                            PORT SERVICE será responsável, de qualquer forma
                            que seja, pelo Conteúdo e/ou pelas perdas ou danos
                            incorridos em função do uso, submissão e acesso de
                            qualquer Conteúdo e/ou Anúncio listado, enviados por e-
                            mail ou, de qualquer outro modo, disponibilizados através
                        do site.</li>
                        <li>2.1.2. Mesmo ciente de que a PORT SERVICE realizara
                            triagem prévia do Conteúdo e o submete a um processo
                            prévio de aprovação, o Usuário reconhece que a PORT
                            SERVICE terá o direito (mas não a obrigação) de, a seu
                            exclusivo critério, recusar, excluir ou mover qualquer
                            Conteúdo e/ou Anúncio disponibilizado no site, seja por
                            violação aos TCUA ou por quaisquer outras razões de
                        ordem ética, legal ou moral.</li> </ul>
                        <li> 2.2. O Usuário é o único responsável por todo o Anúncio
                            e/ou Conteúdo de sua autoria ou por ele divulgado,
                            transmitido por e-mail ou associado a link disponibilizado
                            no/ou através do site; assim como pelo seu uso,
                            divulgação ou publicação, devendo o Usuário manter a
                            PORT SERVICE, seus funcionários, parceiros e
                            controladores livres e indenes de toda e qualquer
                            conseqüência advinda de seus atos/ omissões, inclusive
                        em relação a terceiros.</li>
                        <ul>
                            <li>2.2.1. Com relação ao Anúncio e/ou Conteúdo divulgado,
                                transmitido ou associado a link no Site, o Usuário declara
                            que:</li>
                            <li>(i) É o titular dos direitos e/ou possui as autorizações
                                necessárias para divulgar o Conteúdo e permitir que a
                                PORT SERVICE o utilize e/ou eventualmente o formate
                            para melhor disposição no Site, e;</li>
                            <li>(ii) Possui o consentimento expresso, licença e/ou
                                permissão de toda e qualquer pessoa identificável no
                            Conteúdo, seja ela para usar seu nome e/ou sua imagem.</li>
                        </ul>
                        <li> 2.3. PORT SERVICE não ratifica, edita, aprova ou
                            monitora previamente qualquer comentário, opinião ou
                            recomendação expressos no Site e não assume
                            quaisquer responsabilidades a eles relacionadas.
                            <ul>
                                <li> 2.3.1. PORT SERVICE se reserva o direito de remover o
                                    Anúncio e/ou qualquer Conteúdo submetido ao Site sem
                                    aviso prévio, e poderá, a seu exclusivo critério, suspender
                                    ou cancelar o cadastro e/ou acesso do usuário ao site
                                    caso ocorra a infração a qualquer das disposições deste
                                    TCUA.  PORT SERVICE também se reserva o direito de
                                    decidir, a seu único e exclusivo critério, quais Conteúdos
                                e/ou Anúncios entende como inadequados.</li>
                            </ul>
                        </li>
                        <li> 2.4. No caso de remoção de Conteúdo e/ou Anúncio por
                            desrespeito às Condições de Uso deste  TCUA,
                            eventuais taxas pagas pelo destaque do anúncio ou
                        qualquer outro serviço, não serão reembolsadas.</li>
                    </ul> <br>
                    3. <b>POLÍTICA DE CONTEÚDO E PENALIDADES</b> <br>
                    <ul>
                        <li> 3.1. PORT SERVICE não permite, e desde já considera
                            como inadequado quaisquer Anúncios e/ou Conteúdos
                            enquadrados na lista abaixo. Desta forma, o Usuário
                            compromete-se a não divulgar, enviar por e-mail, ou
                            disponibilizar de qualquer outro modo, Conteúdo e/ou
                            anúncio:
                            <ul>
                                <li>
                                (i)Ilícito;</li>
                                <li>
                                    (ii)Protegido por direitos autorais, segredo comercial,
                                    industrial ou de terceiros; a menos que o Usuário tenha
                                    permissão do titular de tais direitos para divulgar o
                                Conteúdo e/ou Anúncio;</li>
                                <li>
                                    (iii)Nocivo, abusivo, difamatório, pornográfico, libidinoso
                                    ou que de qualquer forma represente assédio, invasão de
                                privacidade ou risco a menores;</li>
                                <li>(iv)Que represente assédio, degradação, intimidação ou
                                    ódio em relação a um indivíduo ou grupo de indivíduos
                                    com base na religião, sexo, orientação sexual, raça,
                                origem ética, idade ou deficiência;</li>
                                <li>(v)Que inclua informações pessoais ou que permitam a
                                    identificação de terceiro sem seu expresso
                                consentimento;</li>
                                <li>(vi)Falso, fraudulento, enganoso ou que represente venda
                                casada ou promoção enganosa;</li>
                                <li>(vii)Que represente ou contenha marketing de associação
                                    ilícito, referência a link, spam, correntes ou esquemas de
                                pirâmide;</li>
                                <li>(viii)Que divulgue, mencione ou faça apologia a quaisquer
                                    serviços ilegais ou venda de quaisquer itens cujo
                                    comércio e propaganda sejam proibidos ou restritos por
                                lei;</li>
                                <li>(ix)Que contenha vírus ou qualquer outro código
                                    malicioso, arquivos ou programas projetados para
                                    interromper, destruir ou limitar a funcionalidade de
                                qualquer software ou hardware;</li>
                                <li>(x)Que interrompa o fluxo normal de diálogo com um
                                    número excessivo de mensagens, ou que, de outro
                                    modo, afete de forma negativa a capacidade de outros
                                Usuários de usar os Serviço; ou</li>
                                <li>(xi)Que utilize endereços de e-mail enganosos, títulos
                                    falsos ou, de outro modo, identificadores manipulados
                                    para ocultar a origem do Conteúdo e/ou Anúncio
                                submetido ao site.</li></ul></li>
                                <li>3.1.1. Além disso, o Usuário concorda em não:
                                    (i)Perseguir, ou que qualquer de outro modo, assediar
                                    terceiros;
                                    <ul>
                                        <li>(ii)Obter, guardar, divulgar, comercializar e/ou utilizar
                                            dados pessoais sobre outros Usuários para fins
                                        comerciais ou ilícitos;</li>
                                        <li>(ii)Usar meios automáticos, incluindo spiders, robôs,
                                            crawlers, ferramentas de captação de dados ou similares
                                            para baixar dados do site – exceto quando for o caso de
                                            ferramentas de busca na Internet (ex. Google Search) e
                                        arquivos públicos não comerciais (ex. archive.org);</li>
                                        <li>(iii)Utilizar quaisquer dispositivos automatizados ou
                                            programas de computador que permitam a postagem de
                                            Anúncios e/ou Conteúdos no Site sem que cada um deles
                                            seja inserido manualmente pelo Usuário (dispositivo
                                            automatizado de postagem), inclusive, a título meramente
                                            exemplificativo, o uso de tais dispositivos para envio em
                                            massa de anúncios, ou o envio de anúncios em intervalos
                                        regulares;</li>
                                        <li>(iv)Incluir Links/URLs/Endereços eletrônicos de qualquer
                                            website que direcionem a qualquer outra página na
                                            Internet, seja ela do próprio prestador do serviço, seja um
                                            blog, site de fabricantes, página explicativa, ou qualquer
                                            outra. Da mesma forma o endereço do e-mail do
                                            anunciante só poderá constar no local indicado no
                                            momento do cadastro do anúncio, não poderá de forma
                                            alguma estar disposto na descrição do anúncio, no título
                                        ou nas fotos.</li></ul></li> </ul> <br>
                                        4. <b>ACESSO AO SITE</b> <br>
                                        <ul>
                                            <li>4.1. PORT SERVICE concede ao Usuário uma
                                                autorização revogável a qualquer momento e sem aviso
                                                prévio, não-exclusiva e limitada para acessar o Site e
                                                utilizá-lo de modo pessoal. Esta licença não inclui:
                                                (i)Acesso ao Site por agentes de postagem; e
                                                (ii)Coleta, cópia, duplicação, divulgação ou uso diverso
                                                daquele para o qual foi proposto o Conteúdo em si, bem
                                                como o uso de ferramentas de coleta e/ou análise de
                                                dados, robôs e mecanismo similares para quaisquer
                                                propósitos não autorizados expressamente pela PORT
                                            SERVICE.</li>
                                            <li>4.2. Admite-se uma exceção para a regra do item (ii)
                                                acima no caso de mecanismos de busca de propósito
                                                geral e arquivos públicos não-comerciais que utilizam tais
                                                ferramentas para coletar informações com o único
                                                propósito de divulgar hyperlinks do Site, desde que se
                                                utilize um endereço de IP fixo ou um grupo fixo de
                                                endereços de IP, um agente identificável e seja cumprido
                                                o arquivo robots.txt. “Mecanismos de busca de propósito
                                                geral” não incluem sites ou mecanismos de busca
                                            especializados em listagem de classificados.</li>
                                            <li>4.3. PORT SERVICE permite que o Usuário divulgue em
                                                seu próprio site suas postagens no site
                                                www.portservice.com.br, ou crie um hyperlink para tais
                                                postagens, desde que se destinem à utilização não-
                                                comercial e/ou tão somente a propósitos de relato de
                                            notícias, como blogs e outras mídias pessoais online.</li>
                                            <li>4.4. É permitida a criação de hyperlink para a página
                                                inicial www.portservice.com.br, desde que o link não
                                                denigra, de qualquer forma, a imagem da PORT
                                                SERVICE, de seus empregados ou associados ou que
                                                apresente conteúdo falso, enganoso, depreciativo ou,
                                            ofensivo de qualquer outro modo.</li>
                                            <li>4.5. PORT SERVICE oferece várias seções e/ou
                                                categorias no Site no formato RSS, de modo que os
                                                Usuários poderão incorporar feeds individuais em um site
                                                pessoal, blog ou rede social, bem como visualizar
                                                postagens através de softwares agregadores de notícias,
                                                desde que observadas as seguintes condições:
                                                <ul>
                                                    <li>(i) O uso dos feeds RSS seja tão somente para fins
                                                    pessoais e não comerciais;</li>
                                                    <li>(ii) Cada um dos títulos tenha o link para o anúncio
                                                        originalmente postado no site, para o qual o Usuário será
                                                    diretamente redirecionado;</li>
                                                    <li>(iii) PORT SERVICE seja indicada como fonte junto aos
                                                    feeds RSS;</li>
                                                    <li>(iv) O uso ou divulgação não sugira que a PORT
                                                        SERVICE promova ou apóie causas, idéias, sites,
                                                    produtos e/ou serviços de terceiros;</li>
                                                    <li>(v) Os feeds RSS não sejam redistribuídos; e
                                                        (vi) O uso dos feeds RSS não sobrecarregue os sistemas
                                                    da PORT SERVICE.</li></ul></li>
                                                    4.6. PORT SERVICE poderá encerrar quaisquer feeds, a
                                                    qualquer momento e sem aviso prévio.
                                                    4.7. O uso do Site fora do escopo autorizado e definido
                                                    pela PORT SERVICE implicará na revogação imediata da
                                                    licença aqui concedida sem prejuízo das penalidades
                                                legais.</ul>
                                                5. <b>NOTIFICAÇÃO SOBRE INFRAÇÕES</b> <br>
                                                <ul><br>
                                                    <li>5.1. Caso qualquer pessoa, Usuário ou não, se sentir
                                                        lesado em relação a qualquer Anúncio e/ou Conteúdo,
                                                        poderá encaminhar à PORT SERVICE notificação por
                                                    escrito solicitando sua retirada.</li>
                                                    <li>5.2. As notificações deverão ainda conter as seguintes
                                                        informações:
                                                        <ul>
                                                            <li>a)Assinatura física ou eletrônica da pessoa supostamente
                                                                lesada, ou, se for o caso, do titular do direito intelectual
                                                            violado;</li>
                                                            <li>b)Identificação do objeto protegido por direitos
                                                            intelectuais que tenha sido violado, se for o caso;</li>
                                                            <li>c)Identificação do material que supostamente representa
                                                                a infração, código do(s) Anúncio(s) e/ou link do(s)
                                                                Anúncio(s), ou, em caso de não se tratarem de Anúncios,
                                                                informações necessárias para a devida identificação do
                                                            Conteúdo;</li>
                                                            <li>d)Declaração de que o notificante possui elementos
                                                            suficientes para embasar a alegação de violação legal; </li>
                                                            <li>e)Declaração de que as informações contidas na
                                                                notificação são precisas e verdadeiras, sob pena de
                                                                incorrer nas conseqüentes responsabilidades cíveis e
                                                                penais, e de que o notificante está autorizado a agir em
                                                            nome do titular do direito supostamente violado.</li>
                                                            <li>f)As notificações deverão ser encaminhadas
                                                            àfaleconosco@portservice.com.br.</li>
                                                            <li>g)O notificante reconhece que caso não cumpra com
                                                                todos os requisitos descritos no item, sua notificação
                                                            poderá não ser considerada.</li> </ul></li>
                                                        </ul><br>
                                                        6. <b>POLÍTICA DE PRIVACIDADE E DIVULGAÇÃO DE
                                                        INFORMAÇÕES</b> <br> <br>
                                                        <ul>
                                                            <li>
                                                                6.1. O Usuário desde já autoriza a PORT SERVICE, a
                                                                seu critério, preservar, armazenar todos os Anúncios e/ou
                                                                Conteúdos submetidos ao site, bem como todos os seus
                                                                dados pessoais, a exemplo de endereços de e-mail,
                                                                endereços de IP (Internet Protocol), informações de data
                                                                e horário de acesso, entre outras informações. O Usuário
                                                                também autoriza a PORT SERVICE a informar e/ou
                                                                divulgar estes dados em caso de exigência legal ou se
                                                                razoavelmente necessárias para: cumprir com o devido
                                                                processo legal; fazer cumprir estes TCUA; responder a
                                                                alegações de suposta violação de direitos de terceiros e
                                                                de divulgação indevida de informações para contato de
                                                                terceiros (por exemplo, número de telefone, endereço
                                                                residencial), e para proteger os direitos, a propriedade ou
                                                                a segurança de terceiros ou da própria a PORT SERVICE
                                                            e de seus Usuários.</li>
                                                            <li>6.2. PORT SERVICE  poderá utilizar empresas de
                                                                processamento de cartões de crédito e/ou de gestão de
                                                                meios de pagamentos online terceirizadas, assim como
                                                                empresas para monitorar o tráfego do Site, que, em
                                                                alguns casos, poderão armazenar e coletar informações
                                                            e dados do Usuário.</li>
                                                            <li>6.3. A utilização do Site implica no consentimento do
                                                                Usuário para coleta, armazenamento e uso das
                                                                informações pessoais fornecidas e suas atualizações,
                                                            dados de tráfego, endereços IP, entre outros.</li>
                                                            <li>6.4.  PORT SERVICE  poderá utilizar cookies para
                                                                administrar as sessões dos Usuários e armazenar
                                                                preferências, rastrear informações, idioma selecionado,
                                                                entre outros. Cookies poderão ser utilizados
                                                                independentemente de cadastro do Usuário.
                                                                <ul>
                                                                    <li>6.4.1. “Cookies” são pequenos arquivos de texto
                                                                        transferidos via servidor para o disco rígido e
                                                                        armazenados no computador do Usuário. Cookies podem
                                                                        coletar informações como data e horário de acesso,
                                                                    histórico de navegação, preferências e nome do usuário.</li>
                                                                    <li>6.4.2. Em alguns casos, prestadores de serviços
                                                                        terceirizados poderão utilizar cookies no Site, que a
                                                                        PORT SERVICE não controla nem acessa. Tais cookies
                                                                    não são regulados por estes TCUA.</li></ul><li>
                                                                    <li> 6.5. O Usuário tem a opção de aceitar ou recusar o uso
                                                                        de cookies em seu computador, independente de
                                                                        cadastro no Site, configurando seu navegador como
                                                                        desejar. A recusa do uso de cookies pode, contudo,
                                                                        resultar na limitação do acesso do Usuário a certas
                                                                        ferramentas disponíveis no Site, dificuldades no login e
                                                                    na ferramenta de comentários.</li>
                                                                    <li>6.6. Considerando que a PORT SERVICE poderá realizar
                                                                        parcerias com terceiros, eventualmente estes poderão
                                                                        coletar informações de usuários como endereço IP,
                                                                        especificação do navegador e sistema operacional. Estes
                                                                        Termos e Condições não se aplicam às informações
                                                                        pessoais fornecidas aos terceiros e por eles
                                                                    armazenadas e utilizadas.</li>
                                                                    <li>6.7. O Site poderá, eventualmente, conter links para sites
                                                                        de terceiros. A PORT SERVICE não se responsabiliza
                                                                        pelo conteúdo ou pela segurança das informações do
                                                                        Usuário quando acessar sites de terceiros. Tais sites
                                                                        podem possuir suas próprias políticas de privacidade
                                                                        quanto ao armazenamento e conservação de
                                                                        informações pessoais, completamente alheias à PORT
                                                                    SERVICE.</li>
                                                                    <li>6.8. PORT SERVICE poderá trabalhar com empresas
                                                                        terceirizadas de propaganda para a divulgação de
                                                                        anúncios durante visitas a seu site. Tais empresas
                                                                        poderão coletar informações sobre as visitas de Usuários
                                                                        ao site www.portservice.com.br, no intuito de fornecer
                                                                        anúncios personalizados sobre bens e serviços do
                                                                        interesse do Usuário. Tais informações não incluem nem
                                                                        incluirão nome, endereço, e-mail ou número de telefone
                                                                        do Usuário.
                                                                        <ul>
                                                                            <li>6.8.1. No site www.portservice.com.br , fornecedores
                                                                                terceirizados poderão utilizar cookies para divulgar
                                                                                anúncios relacionados com os Anúncios e/ou Conteúdos.
                                                                                O uso de cookies DART permite a divulgação de
                                                                                anúncios aos Usuários com base em suas visitas ao site
                                                                            e a outros websites.</li>
                                                                            <li>6.8.2. Os Usuários poderão optar por desativar o uso do
                                                                                cookie DART acessando o link:
                                                                                http://www.google.com/privacy_ads.html. Também é
                                                                                possível optar por não ser alvo de propaganda para todas
                                                                                as redes de anúncios a membros NAI visitando:
                                                                            http://www.networkadvertising.org/.</li></ul></li>
                                                                            <li>6.9. Ao publicar um anúncio no site
                                                                                www.portservice.com.br, o Usuário compreende que os
                                                                                anúncios e o Conteúdo são públicos e acessíveis a
                                                                                terceiros, podendo ser listados nos resultados de
                                                                                ferramentas de pesquisa como Yahoo!, MSN, Google e
                                                                                outros e no cache dessas ferramentas de pesquisa, em
                                                                                feeds e outros websites por acordos comerciais entre a
                                                                                PORT SERVICE e outras prestadoras de serviço. O
                                                                                Usuário compreende que é de responsabilidade de cada
                                                                                uma dessas ferramentas de pesquisa, websites ou
                                                                                recursos RSS manter seus índices e cache atualizados e
                                                                                remover o Conteúdo de seus índices e cache, pois não é
                                                                                possível que a PORT SERVICE exerça qualquer
                                                                            influência sobre eles.</li>
                                                                            <li>6.10. PORT SERVICE poderá utilizar as informações
                                                                                e/ou dados fornecidos e/ou coletados pelo usuário para:
                                                                                <ul>
                                                                                    <li>(i) Aplicar estes TCUA;</li>
                                                                                    <li>(ii) Acompanhar as atividades do Usuário, utilizando
                                                                                        palavras chave de pesquisas e novos anúncios para
                                                                                    administrar mais efetivamente o tráfego no Site;</li>
                                                                                    <li>(ii) Prestar serviços ao Usuário;</li>
                                                                                    <li>(iv) Criar e administrar Contas de Usuário;</li>
                                                                                    <li>(v) Prestar assistência técnica.</li></ul></li>
                                                                                    <li>6.11. PORT SERVICE poderá, ainda, compartilhar
                                                                                        informações coletadas, como a capacidade do navegador
                                                                                        ou o sistema operacional do Usuário, com os prestadores
                                                                                        de serviços terceirizados, no intuito de compreender
                                                                                        melhor quais anúncios e serviços podem ser de interesse
                                                                                    do Usuário.</li>
                                                                                    <li>6.12. PORT SERVICE se reserva o direito de reter
                                                                                        informações pelo período que entender necessário para o
                                                                                        bom cumprimento de seus negócios, mesmo após o
                                                                                        encerramento da conta do Usuário, salvo manifestação
                                                                                    expressa do Usuário em sentido contrário.</li>
                                                                                    <li>6.13. PORT SERVICE não processa pagamentos
                                                                                        diretamente e não armazena informações de cartões de
                                                                                        crédito dos Usuários. É utilizada a tecnologia SSL
                                                                                        (secured socket layer) para o processamento de
                                                                                    transações com prestadores de serviços terceirizados.</li>
                                                                                    <li>6.14. As informações e dados cadastrais do Usuário
                                                                                        podem ser protegidos por senha e nome de Usuário
                                                                                        exclusivos, pessoais e intransferíveis, que não devem ser
                                                                                        divulgadas, quer por PORT SERVICE, quer pelo próprio
                                                                                    Usuário.</li>
                                                                                    <li>6.15. O Usuário desde já declara estar ciente que  PORT
                                                                                        SERVICE não assume nenhuma responsabilidade em
                                                                                        caso de roubo, perda, alteração ou uso indevido de suas
                                                                                        informações pessoais e do Conteúdo como um todo,
                                                                                        inclusive na hipótese de informações fornecidas a
                                                                                        prestadores de serviços terceirizados ou a outros
                                                                                        Usuários e na hipótese de descumprimento destes TCUA
                                                                                    por prestadores de serviços terceirizados.</li>
                                                                                    <li>6.16. O Usuário precisará se cadastrar para comentar ou
                                                                                        ranquear os serviços prestados nos anúncios do site. Ao
                                                                                        utilizar o site www.portservice.com.br   para realizar tais
                                                                                        comentários ou ranqueamento do prestador de serviços,
                                                                                        o Usuário manifesta aceite aos Termos e Condições de
                                                                                    Uso e Aceite.</li>
                                                                                    <li>6.17. O Usuário poderá optar por não receber via e-mail
                                                                                        determinadas mensagens, como newsletters,
                                                                                        recomendações de serviços do site e avisos para
                                                                                        atualização de suas mensagens, para isto deverá entrar
                                                                                        em contato com a administração do site e manifestar sua
                                                                                    opção através do e-mail faleconosco@portservice.com.br.</li>
                                                                                </ul> <br>
                                                                                7. <b>DIREITOS DE PROPRIEDADE INTELECTUAL</b> <br> <br>
                                                                                <ul>
                                                                                    <li>7.1. Os elementos e/ou ferramentas encontrados no site,
                                                                                        com exceção dos Conteúdos e/ou Anúncios submetidos
                                                                                        por Usuários são de titularidade ou licenciados por
                                                                                        PORT SERVICE, sujeitos aos direitos intelectuais de
                                                                                        acordo com as leis brasileiras e tratados e convenções
                                                                                        internacionais dos quais o Brasil seja signatário. Apenas
                                                                                        a título exemplificativo, entendem-se como tais: textos,
                                                                                        softwares, scripts, imagens gráficas, fotos, sons, músicas,
                                                                                        vídeos, recursos interativos e similares, marcas, marcas
                                                                                    de serviços e logotipos.</li>
                                                                                    <li>7.2. Os elementos e/ou ferramentas que são
                                                                                        disponibilizados para o Usuário no site os são tão e
                                                                                        somente no ESTADO EM QUE SE ENCONTRAM e
                                                                                        apenas para sua informação e uso pessoal na forma
                                                                                        designada pela PORT SERVICE. Tais elementos e/ou
                                                                                        ferramentas não podem ser usados, copiados,
                                                                                        reproduzidos, distribuídos, transmitidos, difundidos,
                                                                                        exibidos, vendidos, licenciados ou, de outro modo,
                                                                                        explorados para quaisquer fins, sem o consentimento
                                                                                    prévio e por escrito da PORT SERVICE.</li>
                                                                                    <li>7.3. PORT SERVICE  reserva a si todos os direitos que
                                                                                        não foram expressamente previstos em relação ao Site,
                                                                                        aos seus elementos e/ou ferramentas. O usuário
                                                                                        compromete-se a não usar, reproduzir ou distribuir
                                                                                        quaisquer elementos e/ou ferramentas que não sejam
                                                                                        expressamente permitidos pela PORT SERVICE  –
                                                                                        inclusive o uso, reprodução ou distribuição para fins
                                                                                        comerciais dos Anúncios e/ou Conteúdos extraídos do
                                                                                        site. Caso o Usuário faça qualquer cópia, seja ela via
                                                                                        download ou impressão, dos elementos e/ou ferramentas
                                                                                        de PORT SERVICE para uso exclusivamente pessoal,
                                                                                        deverá preservar todos os direitos de propriedade
                                                                                        intelectual inerentes. O Usuário concorda em não burlar,
                                                                                        desativar ou, de alguma forma, interferir em recursos e/ou
                                                                                        ferramentas relacionados à segurança do site, sob pena
                                                                                    de incorrer nas medidas judiciais cabíveis.</li>
                                                                                </ul> <br>
                                                                                8. <b>GARANTIA DE ANÚNCIO E/OU CONTEÚDO</b> <br> <br>
                                                                                <ul><li>
                                                                                    8.1. O Usuário está ciente de que, ao acessar o Site
                                                                                    www.portservice.com.br , (i) estará exposto a Anúncios
                                                                                    e/ou Conteúdos de uma variedade de fontes não
                                                                                    validadas ou garantidas previamente por PORT
                                                                                    SERVICE, e (ii) que PORT SERVICE  não é responsável
                                                                                    pela sua precisão, veracidade, legalidade, licitude,
                                                                                    utilidade e/ou segurança destes Anúncios e/ou
                                                                                Conteúdos.</li>
                                                                            </ul> <br>
                                                                            9. <b>LIMITAÇÃO DE RESPONSABILIDADE</b> <br> <br>
                                                                            <ul><li>
                                                                                O Usuário se compromete a defender, indenizar e isentar
                                                                                de qualquer responsabilidade PORT SERVICE, seus
                                                                                executivos, subsidiárias, afiliados, sucessores, prepostos,
                                                                                diretores, agentes, prestadores de serviços, fornecedores
                                                                                e funcionários com relação a toda e qualquer reclamação,
                                                                                perda, dano, obrigação, custos, dívidas ou despesas
                                                                                (incluindo, entre outros, honorários advocatícios e custas
                                                                                processuais) incorridas em razão de:
                                                                                <ul>
                                                                                    <li>(i) mau uso e
                                                                                    acesso do Site www.portservice.com.br;</li>
                                                                                    <li>(ii) violação e/ou
                                                                                    não observância de quaisquer dispositivos destes T&C;</li>
                                                                                    <li>(iii) violação de qualquer direito de terceiros, incluindo,
                                                                                        mas sem se limitar a, qualquer direito proprietário ou de
                                                                                    privacidade; ou</li>
                                                                                    <li>(iv) qualquer reclamação de que algum
                                                                                        Anúncio e/ou Conteúdo causou dano a terceiros. Esta
                                                                                        defesa e obrigação de indenização subsistirão a estes
                                                                                    TCUA e ao uso e/ou acesso do site.</li></ul></li></ul> <br>
                                                                                    10. <b>POLÍTICA ANTI-SPAM</b> <br> <br>
                                                                                    <ul>
                                                                                        <li>O Usuário está ciente de que o envio, por e-mail ou pelos
                                                                                            sistemas de computadores da PORT SERVICE de
                                                                                            mensagens não solicitadas aos endereços de e-mail de
                                                                                            PORT SERVICE e/ou de sua base de dados é
                                                                                            expressamente proibido por estes TCUA. O uso não
                                                                                            autorizado dos sistemas de computadores da PORT
                                                                                            SERVICE representa uma violação a estes TCUA e à
                                                                                            legislação brasileira. Tais violações podem sujeitar o
                                                                                            remetente e seus agentes às penalidades cíveis e
                                                                                        criminais aplicáveis.</li>
                                                                                    </ul><br>
                                                                                    11. <b>TRANSAÇÕES ENTRE USUÁRIOS E ENTRE</b> <br> <br>
                                                                                    <ul><li> USUÁRIOS E TERCEIROS
                                                                                        <ul>
                                                                                            <li>11.1. PORT SERVICE não será responsável pelos
                                                                                                negócios e/ou transações, efetivadas ou não, entre
                                                                                                Usuários ou entre Usuários e terceiros. Apenas a título
                                                                                                exemplificativo, entendem-se como tais negócios: o
                                                                                                pagamento e entrega de bens e serviços e quaisquer
                                                                                                outros termos, condições, garantias ou representações
                                                                                                associadas a essas transações. Tais transações são
                                                                                                realizadas entre Usuários ou entre Usuários e terceiros, e
                                                                                                portanto de sua exclusiva responsabilidade, sem
                                                                                                qualquer envolvimento ou participação da PORT
                                                                                            SERVICE.</li>
                                                                                            <li>11.2. O usuário está ciente de que a PORT SERVICE
                                                                                                não deve ser responsabilizada ou sujeitar-se a quaisquer
                                                                                                perdas e/ou danos incorridos em face destas transações.
                                                                                                Os usuários concordam que, em caso de litígio entre
                                                                                                Usuários ou entre Usuários e terceiros, PORT SERVICE
                                                                                                não será, de qualquer forma, envolvida ou
                                                                                            responsabilizada.</li></ul></li></ul> <br>
                                                                                            12. <b>LIMITAÇÃO E TÉRMINO DO SERVIÇO</b> <br> <br>
                                                                                            <ul>
                                                                                                <li>12.1. O Usuário reconhece que a PORT SERVICE  pode
                                                                                                    estabelecer limites referentes ao uso e acesso do site,
                                                                                                    inclusive quanto ao número máximo de dias que um
                                                                                                    anúncio será exibido no site, o número e tamanho
                                                                                                    máximo de textos, mensagens de e-mail ou outros
                                                                                                    Anúncios e/ou Conteúdos que possam ser transmitidos
                                                                                                ou armazenados no site.</li>
                                                                                                <li>12.2. O Usuário reconhece que a PORT SERVICE  não é
                                                                                                    obrigada a manter ou armazenar qualquer Anúncio e/ou
                                                                                                    Conteúdo no Site, podendo, a qualquer momento e por
                                                                                                    qualquer motivo, modificar ou descontinuar o Serviço,
                                                                                                    sem aviso prévio, e sem que qualquer obrigação seja
                                                                                                    criada com os Usuários ou com terceiros por tais
                                                                                                    modificações, suspensões ou descontinuidade do
                                                                                                Serviço.</li>
                                                                                                <li>12.4. O usuário concorda que a PORT SERVICE, a seu
                                                                                                    critério, pode excluir ou desativar sua conta, bloquear seu
                                                                                                    e-mail ou endereço de IP, ou, de outro modo, encerrar
                                                                                                    seu acesso ou uso do Site (ou parte dele) imediatamente,
                                                                                                    sem aviso prévio, além de remover qualquer Anúncio
                                                                                                e/ou Conteúdo do site.</li>
                                                                                                <li>12.5. O usuário reconhece ainda, que a PORT SERVICE
                                                                                                    não possui nenhuma obrigação com ele ou com terceiros
                                                                                                    pelo término de seu acesso ao site e concorda em não
                                                                                                tentar acessá-lo novamente.</li></ul> <br>
                                                                                                13. <b>TRANSFERÊNCIA DE DIREITOS</b> <br><br>
                                                                                                <ul>
                                                                                                    <li>13.1. Estes TCUA, bem como quaisquer direitos, licenças
                                                                                                        e/ou autorizações implícitas ou expressas concedidas
                                                                                                        por  PORT SERVICE  aos seus Usuários, não podem ser
                                                                                                    cedidos, ou de qualquer forma transferidos, pelo Usuário.</li></ul>
                                                                                                    <br>
                                                                                                    14. <b>CAPACIDADE DO USUÁRIO EM USAR OS
                                                                                                    SERVIÇOS OFERECIDOS PELO SITE</b> <br> <br>
                                                                                                    <ul>
                                                                                                        <li>14.1. O Usuário, se pessoa física, declara ser maior de
                                                                                                            18 anos, menor emancipado, ou possuir o consentimento
                                                                                                            legal, expresso e por escrito, dos pais ou responsável
                                                                                                            legal e ser plenamente capaz para se vincular a estes
                                                                                                        TCUA, acatar e cumprir com suas disposições.</li>
                                                                                                        <li>14.2. Menores de idade não devem enviar informações
                                                                                                            pessoais, tais como endereço de e-mail, nome e/ou
                                                                                                            informação para contato através do site
                                                                                                            www.portservice.com.br.  Apenas os pais ou
                                                                                                            responsáveis legais estão autorizados a entrar em
                                                                                                        contato com a PORT SERVICE.</li>
                                                                                                    </ul>
                                                                                                    
                                                                                                    <br>
                                                                                                    15. <b>REGRAS GERAIS, LEGISLAÇÃO APLICÁVEL E
                                                                                                    FORO</b> <br><br>
                                                                                                    <ul>
                                                                                                        <li>15.1. Estes TCUA e quaisquer outras políticas divulgadas
                                                                                                            pela PORT SERVICE no site estabelecem o pleno e
                                                                                                            completo acordo e entendimento entre o Usuário
                                                                                                            superando e revogando todos e quaisquer
                                                                                                            entendimentos, propostas, acordos, negociações e
                                                                                                        discussões havidos anteriormente entre as partes.</li>
                                                                                                        <li>15.2. Os TCUA e a relação entre as partes são regidos
                                                                                                        pelas leis da República Federativa do Brasil.</li>
                                                                                                        <li>15.3. A incapacidade da PORT SERVICE em exercer ou
                                                                                                            fazer cumprir qualquer direito ou cláusula dos TCUA não
                                                                                                        representa uma renúncia desse direito ou cláusula.</li>
                                                                                                        <li>15.4. Na hipótese de que qualquer item, termo ou
                                                                                                            disposição destes TCUA vir a ser declarado nulo ou não
                                                                                                            aplicável, tal nulidade ou inexequibilidade não afetará
                                                                                                            quaisquer outros itens, termos ou disposições aqui
                                                                                                        contidos, os quais permanecerão em pleno vigor e efeito.</li>
                                                                                                    </ul>
                                                                                                    16. <b>SERVIÇOS NÃO PERMITIDOS PELA PORT SERVICE:</b> <br><br>
                                                                                                    <ul><li>
                                                                                                        Somente poderão ser veiculados no site
                                                                                                        www.portservice.com.br  anúncios de serviços permitidos
                                                                                                        pelos TCUA da PORT SERVICE e pela lei vigente. Os
                                                                                                        anunciantes são responsáveis pelos serviços oferecidos
                                                                                                        e devem certificar-se da autenticidade e legalidade de
                                                                                                        seus anúncios. PORT SERVICE  poderá cancelar
                                                                                                        anúncios que infrinjam os TCUA de sua plataforma ou a
                                                                                                        da Lei Vigente. <br>
                                                                                                        O anunciante tem plena ciência de que seus dados de
                                                                                                        cadastro e/ou dados de acesso serão informados se
                                                                                                        solicitados por autoridades competentes (polícia,
                                                                                                        agências reguladoras, ministério público, ordem judicial).
                                                                                                        Ao cadastrar um anúncio o usuário deve verificar a
                                                                                                    listagem de produtos proibidos:</li>
                                                                                                    <li>
                                                                                                        16.1. Serviços ou instruções para importação de bens
                                                                                                        que resultem em evasão fiscal</li>
                                                                                                       <li> 16.2. Produtos que violem leis sobre direito de autor,
                                                                                                        patente, marcas, modelos, desenhos industriais ou
                                                                                                        segredos industriais. Ficam desde já expressamente,
                                                                                                        mas não unicamente, proibidos.</li>
                                                                                                       <li> 16.3. Serviços de prostituição e/ou similares.</li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                                <br />
                                                                                            </fieldset>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        </div><!-- Modal -->
                                                                        <script src="js/jquery.js"></script>
                                                                        <script src="js/usuario.js"></script>
                                                                        <?php include 'footer.php'; ?>
                                                                    </body>
                                                                    <!-- Mirrored from templates.expresspixel.com/bootlistings/register.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Feb 2016 10:59:57 GMT -->
                                                                </html>