<!-- start:left sidebar -->
<aside class="left-side sidebar-offcanvas">
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="user">
                    <div class="image">
                        <img src="img/user.jpg" class="img-circle" alt="User Image" />
                    </div>
                    <div class="info">
                        <p>Ola, <?php echo $_SESSION['usuario']['nome']; ?></p>
                        <small style="color: #eee;">Bem vindo de volta</small>
                    </div>
                </div>
                <div class="tab-pane" id="notif">
                    <ul class="menu">
                        <li><!-- start message -->
                            <a href="#">
                                <div class="pull-left">
                                    <img src="img/avatar3.png" class="img-circle" alt="User Image"/>
                                </div>
                                <p>
                                    Support Team<br>
                                    <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                </p>
                            </a>
                        </li><!-- end message -->
                        <li>
                            <a href="#">
                                <div class="pull-left">
                                    <img src="img/avatar2.png" class="img-circle" alt="user image"/>
                                </div>
                                <p>
                                    Admin Team<br>
                                    <small><i class="fa fa-clock-o"></i> 2 hours</small>
                                </p>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="tab-pane" id="profile">
                    <div class="text-center profile">
                        <p>Complete your Profile</p>
                        <div class="progress">
                            <div class="progress-bar progress-bar-success progress-bar-striped active"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
                            </div>
                        </div>
                        <p>Next Step : <a href="#">Got 320 Points</a></p>
                    </div>
                </div>
            </div>

        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="active">
                <a href="dashboard.php">
                    <i class="fa fa-dashboard"></i> <span><b>Dashboard</b></span>
                </a>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-image"></i>
                    <span>Anuncios</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="listar_anuncios.php"><i class="fa fa-check-square-o"></i> Pendentes</a></li>
                </ul>
            </li> 
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cog"></i>
                    <span>Cadastros</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="listar_categorias.php"><i class="fa fa-sitemap"></i> Categorias</a></li>
                    <li><a href="listar_filtros.php"><i class="fa fa-filter"></i> Filtros</a></li>
                    <li><a href="listar_planos.php"><i class="fa fa-book"></i> Planos</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-lock"></i>
                    <span>Acesso</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="dashboard.php"><i class="fa fa-group"></i> Usuarios</a></li>
                    <li><a href="dashboard.php"><i class="fa fa-shield"></i> Perfil</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-money"></i>
                    <span>Pagamentos</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="listar_pendentes.php"><i class="fa fa-dollar"></i> Pendentes</a></li>
                </ul>
            </li> 
                         
        </ul>
        <!-- start:project stats -->
        <br>
    </section>
</aside>