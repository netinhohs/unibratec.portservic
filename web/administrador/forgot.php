<!DOCTYPE html>
<html lang="en">
<?php 
    session_start("backoffice");

    include 'head.php'; 
    include 'util/util.php';
    
    if($acao == 'senha'){

        $request = array('email' => $email);

        $ch = curl_init($_SESSION['caminhoWS'].'UsuarioModel/esqueciSenha');

        curl_setopt($ch, CURLOPT_POST, true);                                                                    
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        
        $result = json_decode(curl_exec($ch), true);
        curl_close($ch);

        foreach ($result as $arrusuario) {
            $usuario = $arrusuario;
        }
        $email = "";
        if(count($usuario) > 0){

            include "util/class.phpmailer.php";

            $nome = $usuario[nome];
            $email = $usuario[email];
            $senha = $usuario[senha];

            $assunto = "PortService | Nova senha";
        
            $ponteiro = fopen ("util/nova_senha.html", "r");
            
            while (!feof ($ponteiro)) {
              $msgemail .= fgets($ponteiro, 4096);
            }
            fclose ($ponteiro);
            $msgemail = str_replace("@senha@", $senha, $msgemail);
            $msgemail = str_replace("@nome@", $nome, $msgemail);

            
            try {     

                $mail = new PHPMailer(true);   
                $mail->SMTPDebug = 1;
                $mail->IsSMTP();
                $mail->SMTPAuth = true;
                $mail->SMTPSecure = "tls";
                $mail->Host = "smtp.gmail.com";
                $mail->Port = 587;
                $mail->Username = 'netinhohs90@gmail.com';  
                $mail->Password = 'senhaunica';   
                $mail->SetFrom('netinhohs90@gmail.com', 'PortService');
                $mail->AddAddress($email, 'PortService');
                $mail->Subject = $assunto;
                $mail->MsgHTML($msgemail);
                $ret = $mail->Send();
            } catch (phpmailerException $e) {
              echo $e->errorMessage(); //Pretty error messages from PHPMailer
            } catch (Exception $e) {
              echo $e->getMessage(); //Boring error messages from anything else!
            }
            
           /* $to = $email;
            $subject = 'PortServic - Pesquise milhares de prestadores, serviços e qualidade em um só lugar';
            $leitura = 'MIME-Version: 1.0' . "\r\n";
            //$Mensagem  .= $msg;
            $leitura .= 'Content-type: text/html; charset=utf-8' . "\r\n";
            $leitura .= 'From: PortServic -  <naoresponda@portservic.com.br>' . "\r\n";
            mail($to, $subject, $msgemail, $leitura);*/

            $msgok = "Um E-Mail foi enviado para você ";
        }else{
            $msgwar = "E-Mail não cadastrado";
        }
    }
?>
<body class="cl-default fixed" id="login">

    <!-- start:wrapper -->
    <div class="header-login">
        <div class="text-center">
            <h1><i class="fa fa-pied-piper-alt"></i>  PortService Esqueçi Senha </h1>
        </div>
    </div>
 
     <div class="body-login">
        <?php include 'util/box_messages.php'; ?>
        <form role="form" id="form1" name="form1">
        <input type="hidden" name="acao" value="senha">
            <div class="form-group">
                <input type="email" name="email" class="form-control input-lg" id="email" placeholder="Enter email">
            </div>
            <button type="submit" class="btn btn-default btn-block btn-lg btn-perspective">Enviar</button>
        </form>
        <div class="text-center">
            <p><a href="index.php"><strong>Voltar</strong></a></p>
        </div>
    </div>  
</body>
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script>

$(document).ready(function () {

     $("#form1").validate({
        focusInvalid: true, 
        rules: {
            email: {
                email: true,
                required: true
            }
        },

        invalidHandler: function (event, validator) {              
            $('#alert-danger').text('Os campos marcados são de preenchimento obrigatório');
            $('#alert-danger').show();
            $('#alert-warning').hide();
            $('#alert-success').hide();
        },

        errorPlacement: function (error, element) { 
            
        },

        highlight: function (element) { 
            $(element)
            .closest('.form-group').addClass('has-error'); 
        },

        unhighlight: function (element) {
            
        },

        success: function (label, element) {
            $(element).closest('.form-group').removeClass('has-error'); 
        },

        submitHandler: function (form) {
            form.submit();
        }
    });
});
</script>
<!-- Mirrored from bootemplates.com/themes/arjuna/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 23 Oct 2015 22:39:08 GMT -->
</html>