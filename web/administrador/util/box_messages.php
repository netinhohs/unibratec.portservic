<div id="alert-danger" class="alert alert-danger alert-dismissible" <?php if(empty($msgerr)){ ?>style="display:none;" <?php } ?> role="alert">
    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <strong>Erro!</strong> <?php echo $msgerr; ?>
</div>

<div id="alert-warning" class="alert alert-warning alert-dismissible" <?php if(empty($msgwar)){ ?>style="display:none;" <?php } ?> role="alert">
    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <strong>Aviso!</strong> <?php echo $msgwar; ?>
</div>

<div id="alert-success" class="alert alert-success alert-dismissible" <?php if(empty($msgok)){ ?>style="display:none;" <?php } ?> role="alert">
    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <strong>Sucesso!</strong> <?php echo $msgok; ?>
</div>