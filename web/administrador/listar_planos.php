<?php 
    session_start("backoffice");

    include 'head.php'; 
    include 'util/util.php'; 

    if($ordem == ""){
        $ordem = "plano_id";
    }

    if($acao == "pesquisar"){
        unset($nome);
    }

?>
<body class="cl-default fixed">
    <?php include 'nav_bar_top.php'; ?>


    <!-- start:wrapper body -->
    <div class="wrapper row-offcanvas row-offcanvas-left">

        <!-- end:left sidebar -->
        <?php include 'nav_menu_left.php'; ?>
        <!-- start:right sidebar -->

        <aside class="right-side">
            <section class="content">
                <h1>
                    Plano
                    <small>Listagem de Planos</small>
                </h1>
                <!-- start:breadcrumb -->
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-cog"></i> Cadastros</a></li>
                    <li><a href="listar_planos.php"> Planos</a></li>
                    <!--<li class="active">Blank Page</li> -->
                </ol>
                <!-- end:breadcrumb -->
                <?php 
                    $filtros = "";
                    $request = array();
                    if($nome != ""){
                        $request['nome'] = $nome;
                    }

                    $ch = curl_init($_SESSION['caminhoWS'].'PlanoModel/consultarRows');

                    curl_setopt($ch, CURLOPT_POST, true);                                                                    
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $request);                                                                  
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                 
                    
                    $qtd = json_decode(curl_exec($ch), true);
                    include("util/paginacao.php");


                    curl_close($ch);

                    if($ordem != ""){
                         $filtros .= "ORDER BY " . str_replace("_desc", " DESC", $ordem);
                    }
                    $filtros .= " LIMIT $inicio, ". $_SESSION['qtdList'];
                    $request['filtros'] = $filtros;


                    $ch = curl_init($_SESSION['caminhoWS'].'PlanoModel/listPlanos');

                    curl_setopt($ch, CURLOPT_POST, true);                                                                    
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $request);                                                                  
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                 
                    
                    $arrPlanos = json_decode(curl_exec($ch), true);

                    curl_close($ch);
                ?>
                <!-- start:content -->
                <form name="form1" id="form1" method="post">
                    <input type="hidden" name="ordem" id="ordem" value="<?php print($ordem) ?>">
                    <input type="hidden" name="pag" id="pag" value="<?php print($pag) ?>">
                    <input type="hidden" name="acao" id="acao">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php include 'util/box_messages.php'; ?>
                            <div class="panel">
                                <div class="panel-body">
                                    <div class="form-inline" role="form">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="nome" class="control-label">Nome:</label>
                                            </div>
                                            <div class="form-group">
                                                <input  type="text" style="width: 230px" class="form-control input-sm" name="nome" id="nome" value="<?php print($nome) ?>">
                                            </div>
                                        </div>
                                        <div class="col-lg-3 pull-right">
                                            <div class="form-group pull-right">
                                                <?php if($nome != ""){ ?>
                                                <button type="button" onClick="Pesquisar('<?php print($ordem) ?>', '', 'S')" class="btn btn-warning"><i class="fa fa-ban"></i> Cancelar</button>
                                            <?php }else{ ?>
                                                <button type="button" onClick="Pesquisar('<?php print($ordem) ?>', '', 'N')" class="btn btn-success"><i class="fa fa-ok"></i>Filtrar</button>
                                            <?php } ?>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-success" data-original-title="" title=""><i class="fa fa-cog"></i> Ações</button>
                                                <button type="button" class="btn btn-success active dropdown-toggle" data-toggle="dropdown" data-original-title="" title="">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="manter_plano.php">Incluir</a></li>
                                                </ul>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box blank-page">
                                <?php include "util/links_paginacao.php"; ?>
                                <div class="col-md-12">
                                    <table class="table table-striped table-advance table-hover">
                                    <thead>
                                        <tr>
                                            <th style="cursor: pointer;" onClick="Pesquisar('plano_id<?php if($ordem == "plano_id"){ ?>_desc<?php } ?>', '');" <?php if($ordem == "plano_id"){ ?>class="fa fa-sort-asc"<?php } else if($ordem == "plano_id_desc"){ ?>class="fa fa-sort-desc"<?php } ?>> ID </th>
                                            <th style="cursor: pointer;" onClick="Pesquisar('nome<?php if($ordem == "nome"){ ?>_desc<?php } ?>', '');" <?php if($ordem == "nome"){ ?>class="fa fa-sort-asc"<?php } else if($ordem == "nome_desc"){ ?>class="fa fa-sort-desc"<?php } ?>> Nome </th>
                                            <th style="cursor: pointer;" onClick="Pesquisar('preco<?php if($ordem == "preco"){ ?>_desc<?php } ?>', '');" <?php if($ordem == "preco"){ ?>class="fa fa-sort-asc"<?php } else if($ordem == "preco_desc"){ ?>class="fa fa-sort-desc"<?php } ?>> Pre&ccedil;o </th>
                                            <th> Status </th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($arrPlanos as $planos => $plano) { ?>
                                        <tr>
                                            <td> <?php echo $plano['plano_id']; ?> </td>
                                            <td> <?php echo $plano['nome']; ?> </td>
                                            <td> <?php echo FormatarValorExibir($plano['preco']); ?> </td>
                                            <td> <?php if($plano['status'] == "A"){ ?> <span class="label label-success label-mini">Ativo</span> <?php }elseif($plano['status'] == "I"){ ?> <span class="label label-warning label-mini">Inativo </span><?php } ?> </td>
                                            <td>
                                                <a class="btn btn-primary btn-xs" data-original-title="Alterar" title="Alterar" href="manter_plano.php?codplano=<?php echo $plano['plano_id']; ?>"><i class="fa fa-pencil" data-original-title="Alterar" title="Alterar"></i></a>
                                                <a class="btn btn-danger btn-xs" data-original-title="Excluir" title="Excluir" href="#"><i class="fa fa-trash-o " data-original-title="Alterar" title="Alterar"></i></a>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                </div>
                                <?php include "util/links_paginacao.php"; ?>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- end:content -->

            </section>
        </aside>
        <!-- end:right sidebar -->

    </div>
    <!-- end:wrapper body -->

</body>
<script type="text/javascript">

function Pesquisar(ordem, pag, ativa){
    $("#ordem").val(ordem);
    $("#pag").val(pag);

    if(ativa == "S"){
        $("#acao").val("pesquisar");        
    }
    $("#form1").submit();
}
</script>

<!-- Mirrored from bootemplates.com/themes/arjuna/blank.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 23 Oct 2015 22:39:08 GMT -->
</html>